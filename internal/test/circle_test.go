package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func TestCircle(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	circleID := uuid.NewString()

	groupID := uuid.NewString()
	group1ID := uuid.NewString()

	now := time.UnixMicro(time.Now().UTC().UnixMicro())
	nowPlus5Minute := now.Add(5 * time.Minute)
	nowPlus10Minute := now.Add(10 * time.Minute)
	nowPlus15Minute := now.Add(15 * time.Minute)
	nowPlus20Minute := now.Add(20 * time.Minute)

	testCases := []struct {
		name        string
		topic       string
		event       event.Event
		expected    repo.Circle
		expectedErr error
	}{
		{
			name: "new_circle_created",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleNew,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.CreateCirclePayload{
					CircleID: circleID,
					GroupID:  groupID,
				},
			},
			expected: repo.Circle{
				CircleKey: repo.CircleKey{
					ID: circleID,
				},
				GroupID:       groupID,
				LastUpdatedAt: &nowPlus5Minute,
			},
		},
		{
			name: "new_circle_created_ignored_when_event_timestamp_is_older_than_last_updated_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleNew,
				Timestamp: uint64(now.UnixMicro()),
				Payload: event.CreateCirclePayload{
					CircleID: circleID,
					GroupID:  group1ID,
				},
			},
			expected: repo.Circle{
				CircleKey: repo.CircleKey{
					ID: circleID,
				},
				GroupID:       groupID,
				LastUpdatedAt: &nowPlus5Minute,
			},
		},
		{
			name: "new_circle_created_applied_when_event_timestamp_is_newer_than_last_updated_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleNew,
				Timestamp: uint64(nowPlus10Minute.UnixMicro()),
				Payload: event.CreateCirclePayload{
					CircleID: circleID,
					GroupID:  group1ID,
				},
			},
			expected: repo.Circle{
				CircleKey: repo.CircleKey{
					ID: circleID,
				},
				GroupID:       group1ID,
				LastUpdatedAt: &nowPlus10Minute,
			},
		},
		{
			name: "circle_group_changed_ignored_when_event_timestamp_is_older_than_last_updated_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.CircleGroupChanged,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.CircleGroupChangedPayload{
					CircleID: circleID,
					GroupID:  groupID,
				},
			},
			expected: repo.Circle{
				CircleKey: repo.CircleKey{
					ID: circleID,
				},
				GroupID:       group1ID,
				LastUpdatedAt: &nowPlus10Minute,
			},
		},
		{
			name: "circle_group_changed_applied_when_event_timestamp_is_newer_than_last_updated_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.CircleGroupChanged,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.CircleGroupChangedPayload{
					CircleID: circleID,
					GroupID:  groupID,
				},
			},
			expected: repo.Circle{
				CircleKey: repo.CircleKey{
					ID: circleID,
				},
				GroupID:       groupID,
				LastUpdatedAt: &nowPlus15Minute,
			},
		},
		{
			name: "circle_deleted_ignored_when_event_timestamp_is_older_than_last_updated_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleDelete,
				Timestamp: uint64(nowPlus10Minute.UnixMicro()),
				Payload: event.DeleteCirclePayload{
					CircleID: circleID,
				},
			},
			expected: repo.Circle{
				CircleKey: repo.CircleKey{
					ID: circleID,
				},
				GroupID:       groupID,
				LastUpdatedAt: &nowPlus15Minute,
			},
		},
		{
			name: "circle_deleted",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleDelete,
				Timestamp: uint64(nowPlus20Minute.UnixMicro()),
				Payload: event.DeleteCirclePayload{
					CircleID: circleID,
				},
			},
			expectedErr: repo.ErrNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := container.bus.Topic(eventbus.TopicID().Circle).Pub(context.Background(), tc.event)
			require.NoError(t, err)

			var circle repo.Circle
			Eventually(t, func() bool {
				circle, err = container.spannerRepo.SingleRead().GetCircle(
					context.Background(),
					repo.CircleKey{
						ID: tc.expected.ID,
					},
				)

				if tc.expectedErr != nil {
					t.Logf("Expected error: '%v', got: '%v'", tc.expectedErr, err)
					return errors.Is(err, tc.expectedErr)
				}

				if err != nil {
					t.Logf("Unexpected error: '%v'", err)
					return false
				}

				if !cmp.Equal(tc.expected, circle) {
					t.Logf(
						"Expected:\n %v\n, \ngot:\n %v\n Diff: %s",
						tc.expected,
						circle,
						cmp.Diff(tc.expected, circle),
					)
					return false
				}

				return true
			})
		})
	}
}
