package service

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) RemovePost(ctx context.Context, postID string, deletedAt time.Time) error {
	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		posts, err := tx.GetPosts(ctx, postID)
		if err != nil || len(posts) == 0 {
			return err
		}

		return tx.RemovePost(ctx, postID, deletedAt)
	})
	return err
}
