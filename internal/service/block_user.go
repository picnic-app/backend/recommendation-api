package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) BlockUser(
	ctx context.Context,
	userID,
	targetUserID string,
	blockedAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if targetUserID == "" {
		return ErrRequiredParam("target user id")
	}

	if blockedAt.IsZero() {
		return ErrRequiredParam("blocked at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exits := true
		edge, err := tx.GetUserUserEdge(ctx, repo.UserUserEdgeKey{
			UserID:       userID,
			TargetUserID: targetUserID,
		})
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				exits = false
			} else {
				return err
			}
		}

		if exits {
			if edge.IsBlocking {
				return nil
			}

			if edge.UnblockedAt == nil || edge.UnblockedAt != nil && edge.UnblockedAt.Before(blockedAt) {
				err = tx.PartialUpdateUserUserEdge(ctx, repo.PartialUpdateUserUserEdgeInput{
					UserUserEdgeKey: repo.UserUserEdgeKey{
						UserID:       userID,
						TargetUserID: targetUserID,
					},
					BlockedAt: &blockedAt,
				})
				if err != nil {
					return err
				}
			}
		} else {
			err = tx.InsertUserUserEdge(ctx, repo.InsertUserUserEdgeInput{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       userID,
					TargetUserID: targetUserID,
				},
				BlockedAt: &blockedAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
