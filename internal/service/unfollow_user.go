package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) UnfollowUser(
	ctx context.Context,
	userID,
	targetUserID string,
	unfollowedAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if targetUserID == "" {
		return ErrRequiredParam("target user id")
	}

	if unfollowedAt.IsZero() {
		return ErrRequiredParam("unfollowed at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exits := true
		edge, err := tx.GetUserUserEdge(ctx, repo.UserUserEdgeKey{
			UserID:       userID,
			TargetUserID: targetUserID,
		})
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				exits = false
			} else {
				return err
			}
		}

		if exits {
			if !edge.IsFollowing {
				return nil
			}

			if edge.FollowedAt == nil || edge.FollowedAt != nil && edge.FollowedAt.Before(unfollowedAt) {
				err = tx.PartialUpdateUserUserEdge(ctx, repo.PartialUpdateUserUserEdgeInput{
					UserUserEdgeKey: repo.UserUserEdgeKey{
						UserID:       userID,
						TargetUserID: targetUserID,
					},
					UnfollowedAt: &unfollowedAt,
				})
				if err != nil {
					return err
				}
			}
		} else {
			err = tx.InsertUserUserEdge(ctx, repo.InsertUserUserEdgeInput{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       userID,
					TargetUserID: targetUserID,
				},
				UnfollowedAt: &unfollowedAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
