package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) DeleteCircle(ctx context.Context, circle repo.CircleKey) error {
	return DeleteCircle(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), circle)
}

func (rw readWrite) DeleteCircle(ctx context.Context, circle repo.CircleKey) error {
	return DeleteCircle(ctx, rw.tx, circle)
}

func DeleteCircle(_ context.Context, db BufferWriter, circle repo.CircleKey) error {
	table := tables.Get().Circles()
	op := spanner.Delete(
		table.TableName(),
		spanner.Key{circle.ID},
	)

	return db.BufferWrite([]*spanner.Mutation{op})
}
