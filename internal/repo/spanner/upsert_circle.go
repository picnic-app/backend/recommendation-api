package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) UpsertCircle(ctx context.Context, circle repo.Circle) error {
	return UpsertCircle(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), circle)
}

func (rw readWrite) UpsertCircle(ctx context.Context, circle repo.Circle) error {
	return UpsertCircle(ctx, rw.tx, circle)
}

func UpsertCircle(_ context.Context, db BufferWriter, circle repo.Circle) error {
	table := tables.Get().Circles()
	op := spanner.InsertOrUpdateMap(
		table.TableName(),
		map[string]interface{}{
			table.Columns().ID():            circle.ID,
			table.Columns().GroupID():       circle.GroupID,
			table.Columns().LastUpdatedAt(): circle.LastUpdatedAt,
		},
	)

	return db.BufferWrite([]*spanner.Mutation{op})
}
