package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func TestUserCircleEdge(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	anotherUserID := uuid.NewString()
	circleID := uuid.NewString()
	now := time.UnixMicro(time.Now().UTC().UnixMicro())
	nowPlus5Minute := now.Add(5 * time.Minute)
	nowPlus10Minute := now.Add(10 * time.Minute)
	nowPlus15Minute := now.Add(15 * time.Minute)

	testCases := []struct {
		name        string
		event       event.Event
		expected    repo.UserCircleEdge
		expectedErr error
	}{
		{
			name: "user_joined_to_circle",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleJoin,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.JoinCirclePayload{
					UserID:   userID,
					CircleID: circleID,
				},
			},
			expected: repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				IsMember:  true,
				JoinedAt:  &nowPlus5Minute,
				LeftAt:    nil,
				CreatedAt: nowPlus5Minute,
			},
		},
		{
			name: "user_left_from_circle_ignored_when_timestamp_is_older_than_joined_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleLeave,
				Timestamp: uint64(now.UnixMicro()),
				Payload: event.LeaveCirclePayload{
					UserID:   userID,
					CircleID: circleID,
				},
			},
			expected: repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				IsMember:  true,
				JoinedAt:  &nowPlus5Minute,
				LeftAt:    nil,
				CreatedAt: nowPlus5Minute,
			},
		},
		{
			name: "user_left_from_circle",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleLeave,
				Timestamp: uint64(nowPlus10Minute.UnixMicro()),
				Payload: event.LeaveCirclePayload{
					UserID:   userID,
					CircleID: circleID,
				},
			},
			expected: repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				IsMember:  false,
				JoinedAt:  &nowPlus5Minute,
				LeftAt:    &nowPlus10Minute,
				CreatedAt: nowPlus5Minute,
			},
		},
		{
			name: "user_joined_to_circle_ignored_when_timestamp_is_older_than_left_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleJoin,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.JoinCirclePayload{
					UserID:   userID,
					CircleID: circleID,
				},
			},
			expected: repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				IsMember:  false,
				JoinedAt:  &nowPlus5Minute,
				LeftAt:    &nowPlus10Minute,
				CreatedAt: nowPlus5Minute,
			},
		},
		{
			name: "user_joined_to_circle_again",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleJoin,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.JoinCirclePayload{
					UserID:   userID,
					CircleID: circleID,
				},
			},
			expected: repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				IsMember:  true,
				JoinedAt:  &nowPlus15Minute,
				LeftAt:    &nowPlus10Minute,
				CreatedAt: nowPlus5Minute,
			},
		},
		{
			name: "another_user_left_to_circle",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeCircleLeave,
				Timestamp: uint64(now.UnixMicro()),
				Payload: event.LeaveCirclePayload{
					UserID:   anotherUserID,
					CircleID: circleID,
				},
			},
			expected: repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   anotherUserID,
					CircleID: circleID,
				},
				IsMember:  false,
				JoinedAt:  nil,
				LeftAt:    &now,
				CreatedAt: now,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := container.bus.Topic(eventbus.TopicID().Circle).Pub(context.Background(), tc.event)
			require.NoError(t, err)

			var edge repo.UserCircleEdge
			Eventually(t, func() bool {
				edge, err = container.spannerRepo.SingleRead().GetUserCircleEdge(
					context.Background(),
					tc.expected.UserCircleEdgeKey,
				)

				if tc.expectedErr != nil {
					t.Logf("Expected error: '%v', got: '%v'", tc.expectedErr, err)
					return errors.Is(err, tc.expectedErr)
				}

				if err != nil {
					t.Logf("Unexpected error: '%v'", err)
					return false
				}

				if !cmp.Equal(tc.expected, edge) {
					t.Logf("Expected:\n %v\n, \ngot:\n %v\n Diff: %s", tc.expected, edge, cmp.Diff(tc.expected, edge))
					return false
				}

				return true
			})
		})
	}
}
