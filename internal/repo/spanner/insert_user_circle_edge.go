package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) InsertUserCircleEdge(ctx context.Context, userCircleEdge repo.UserCircleEdge) error {
	return InsertUserCircleEdge(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), userCircleEdge)
}

func (rw readWrite) InsertUserCircleEdge(ctx context.Context, userCircleEdge repo.UserCircleEdge) error {
	return InsertUserCircleEdge(ctx, rw.tx, userCircleEdge)
}

func InsertUserCircleEdge(_ context.Context, db BufferWriter, userCircleEdge repo.UserCircleEdge) error {
	table := tables.Get().UserCircleEdges()
	op := spanner.InsertMap(
		table.TableName(),
		map[string]interface{}{
			table.Columns().UserID():    userCircleEdge.UserID,
			table.Columns().CircleID():  userCircleEdge.CircleID,
			table.Columns().JoinedAt():  userCircleEdge.JoinedAt,
			table.Columns().LeftAt():    userCircleEdge.LeftAt,
			table.Columns().CreatedAt(): userCircleEdge.CreatedAt,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
