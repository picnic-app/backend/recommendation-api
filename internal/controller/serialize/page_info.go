package serialize

import (
	"github.com/go-openapi/swag"

	"gitlab.com/picnic-app/backend/libs/golang/cursor"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/picnic/common/v1"
)

func PageInfo(page cursor.Page) *v1.PageInfo {
	if page == nil {
		return nil
	}
	return &v1.PageInfo{
		FirstId: swag.String(page.FirstID()),
		LastId:  swag.String(page.LastID()),
		HasPrev: page.HasPrev(),
		HasNext: page.HasNext(),
		Length:  page.Length(),
	}
}
