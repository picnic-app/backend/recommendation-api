package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) InsertUserUserEdge(ctx context.Context, edge repo.InsertUserUserEdgeInput) error {
	return InsertUserUserEdge(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), edge)
}

func (rw readWrite) InsertUserUserEdge(ctx context.Context, edge repo.InsertUserUserEdgeInput) error {
	return InsertUserUserEdge(ctx, rw.tx, edge)
}

func InsertUserUserEdge(_ context.Context, db BufferWriter, edge repo.InsertUserUserEdgeInput) error {
	table := tables.Get().UserUserEdges()
	op := spanner.InsertMap(
		table.TableName(),
		map[string]interface{}{
			table.Columns().UserID():       edge.UserID,
			table.Columns().TargetUserID(): edge.TargetUserID,
			table.Columns().FollowedAt():   edge.FollowedAt,
			table.Columns().UnfollowedAt(): edge.UnfollowedAt,
			table.Columns().BlockedAt():    edge.BlockedAt,
			table.Columns().UnblockedAt():  edge.UnblockedAt,
		},
	)

	return db.BufferWrite([]*spanner.Mutation{op})
}
