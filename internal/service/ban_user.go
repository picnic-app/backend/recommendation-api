package service

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) BanUser(
	ctx context.Context,
	userID string,
	bannedAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if bannedAt.IsZero() {
		return ErrRequiredParam("banned at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		user, err := tx.GetUser(ctx, repo.UserKey{
			UserID: userID,
		})
		if err != nil {
			return err
		}

		if user.IsBanned {
			return nil
		}

		if user.UnbannedAt == nil || (user.UnbannedAt != nil && user.UnbannedAt.Before(bannedAt)) {
			err = tx.PartialUpdateUser(ctx, repo.PartialUpdateUserInput{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				BannedAt: &bannedAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
