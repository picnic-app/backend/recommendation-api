package controller

import (
	"google.golang.org/grpc"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/service"
)

func New(svc *service.Service) Controller {
	return Controller{
		service: svc,
	}
}

// Controller represents struct that implements ProfileServiceServer.
type Controller struct {
	service *service.Service

	v1.UnsafeRecommendationServiceServer
}

func (c Controller) Register(reg grpc.ServiceRegistrar) {
	v1.RegisterRecommendationServiceServer(reg, c)
}
