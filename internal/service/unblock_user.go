package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) UnblockUser(
	ctx context.Context,
	userID,
	targetUserID string,
	unblockedAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if targetUserID == "" {
		return ErrRequiredParam("target user id")
	}

	if unblockedAt.IsZero() {
		return ErrRequiredParam("unblocked at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exits := true
		edge, err := tx.GetUserUserEdge(ctx, repo.UserUserEdgeKey{
			UserID:       userID,
			TargetUserID: targetUserID,
		})
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				exits = false
			} else {
				return err
			}
		}

		if exits {
			if !edge.IsBlocking {
				return nil
			}

			if edge.BlockedAt == nil || edge.BlockedAt != nil && edge.BlockedAt.Before(unblockedAt) {
				err = tx.PartialUpdateUserUserEdge(ctx, repo.PartialUpdateUserUserEdgeInput{
					UserUserEdgeKey: repo.UserUserEdgeKey{
						UserID:       userID,
						TargetUserID: targetUserID,
					},
					UnblockedAt: &unblockedAt,
				})
				if err != nil {
					return err
				}
			}
		} else {
			err = tx.InsertUserUserEdge(ctx, repo.InsertUserUserEdgeInput{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       userID,
					TargetUserID: targetUserID,
				},
				UnblockedAt: &unblockedAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
