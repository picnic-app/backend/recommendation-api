// Code generated by tablegen. DO NOT EDIT.
package tables

func (t AllTables) TempCircleMembers() TempCircleMembers { return TempCircleMembers(t) }

type TempCircleMembers struct{ alias string }

func (t TempCircleMembers) TableAlias() string { return t.alias }
func (t TempCircleMembers) name() string       { return "TempCircleMembers" }
func (t TempCircleMembers) TableName() string  { return tableName(t.name(), "", t.alias) }
func (t TempCircleMembers) AllColumnNames() []string {
	c := t.Columns()
	return []string{
		c.Role(),
		c.UserID(),
		c.BannedAt(),
		c.CircleID(),
		c.JoinedAt(),
		c.UserName(),
		c.BannedTime(),
		c.AddModerators(),
		c.MemberOptions(),
		c.IsCreatedOnBan(),
	}
}

func (t TempCircleMembers) Columns() TempCircleMembersCols {
	return TempCircleMembersCols{tableAlias: t.alias}
}

type TempCircleMembersCols struct{ tableAlias string }

func (c TempCircleMembersCols) Role() string          { return colName(c.tableAlias, "Role") }          // INT64
func (c TempCircleMembersCols) UserID() string        { return colName(c.tableAlias, "UserID") }        // STRING(36)
func (c TempCircleMembersCols) BannedAt() string      { return colName(c.tableAlias, "BannedAt") }      // TIMESTAMP NULL OPTIONS (allow_commit_timestamp = true)
func (c TempCircleMembersCols) CircleID() string      { return colName(c.tableAlias, "CircleID") }      // STRING(36)
func (c TempCircleMembersCols) JoinedAt() string      { return colName(c.tableAlias, "JoinedAt") }      // TIMESTAMP OPTIONS (allow_commit_timestamp = true)
func (c TempCircleMembersCols) UserName() string      { return colName(c.tableAlias, "UserName") }      // STRING(256) DEFAULT ("")
func (c TempCircleMembersCols) BannedTime() string    { return colName(c.tableAlias, "BannedTime") }    // INT64 NULL DEFAULT (0)
func (c TempCircleMembersCols) AddModerators() string { return colName(c.tableAlias, "AddModerators") } // BOOL NULL DEFAULT (FALSE)
func (c TempCircleMembersCols) MemberOptions() string { return colName(c.tableAlias, "MemberOptions") } // JSON NULL DEFAULT (NULL)
func (c TempCircleMembersCols) IsCreatedOnBan() string {
	return colName(c.tableAlias, "IsCreatedOnBan")
} // BOOL NULL DEFAULT (FALSE)
