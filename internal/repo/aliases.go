package repo

type (
	Kind         = int64
	UserID       = string
	PostID       = string
	CircleID     = string
	GroupID      = string
	EntityID     = string
	LanguageCode = string
)
