package test

import (
	"context"
	"testing"

	"cloud.google.com/go/spanner"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/migrations"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/util/slice"
)

func TestCopyPostsMigration(t *testing.T) {
	t.Skip("migration test")

	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	table := tables.Get().TempPosts()
	ops := make([]*spanner.Mutation, 10000)
	for i := 0; i < len(ops); i++ {
		id := uuid.NewString()
		ops[i] = spanner.InsertOrUpdateMap(
			table.TableName(),
			map[string]interface{}{
				table.Columns().ID():           id,
				table.Columns().Kind():         1,
				table.Columns().Title():        id,
				table.Columns().UserID():       id,
				table.Columns().OwnerID():      id,
				table.Columns().CircleID():     id,
				table.Columns().CreatedAt():    spanner.CommitTimestamp,
				table.Columns().OwnerKind():    0,
				table.Columns().LanguageCode(): "en",
			},
		)
	}

	ctx := context.Background()

	err := slice.Batch(ops, 1000, func(batch []*spanner.Mutation) error {
		_, err := container.spannerCli.Apply(ctx, batch, spanner.ApplyAtLeastOnce())
		return err
	})
	require.NoError(t, err)

	err = migrations.NewManager(container.spannerCli, "test").CopyPosts(ctx)
	require.NoError(t, err)
}
