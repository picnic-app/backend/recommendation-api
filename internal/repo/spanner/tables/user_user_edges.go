// Code generated by tablegen. DO NOT EDIT.
package tables

func (t AllTables) UserUserEdges() UserUserEdges { return UserUserEdges(t) }

type UserUserEdges struct{ alias string }

func (t UserUserEdges) TableAlias() string { return t.alias }
func (t UserUserEdges) name() string       { return "UserUserEdges" }
func (t UserUserEdges) TableName() string  { return tableName(t.name(), "", t.alias) }
func (t UserUserEdges) AllColumnNames() []string {
	c := t.Columns()
	return []string{
		c.UserID(),
		c.BlockedAt(),
		c.FollowedAt(),
		c.IsBlocking(),
		c.IsFollowing(),
		c.UnblockedAt(),
		c.TargetUserID(),
		c.UnfollowedAt(),
	}
}

func (t UserUserEdges) Columns() UserUserEdgesCols { return UserUserEdgesCols{tableAlias: t.alias} }

type UserUserEdgesCols struct{ tableAlias string }

func (c UserUserEdgesCols) UserID() string       { return colName(c.tableAlias, "UserID") }       // STRING(36)
func (c UserUserEdgesCols) BlockedAt() string    { return colName(c.tableAlias, "BlockedAt") }    // TIMESTAMP NULL
func (c UserUserEdgesCols) FollowedAt() string   { return colName(c.tableAlias, "FollowedAt") }   // TIMESTAMP NULL
func (c UserUserEdgesCols) IsBlocking() string   { return colName(c.tableAlias, "IsBlocking") }   // BOOL AS (CASE WHEN BlockedAt IS NULL THEN FALSE WHEN BlockedAt IS NOT NULL AND UnblockedAt IS NULL THEN TRUE WHEN BlockedAt IS NOT NULL AND UnblockedAt IS NOT NULL AND BlockedAt > UnblockedAt THEN TRUE ELSE FALSE END) STORED
func (c UserUserEdgesCols) IsFollowing() string  { return colName(c.tableAlias, "IsFollowing") }  // BOOL AS (CASE WHEN FollowedAt IS NULL THEN FALSE WHEN FollowedAt IS NOT NULL AND UnfollowedAt IS NULL THEN TRUE WHEN FollowedAt IS NOT NULL AND UnfollowedAt IS NOT NULL AND FollowedAt > UnfollowedAt THEN TRUE ELSE FALSE END) STORED
func (c UserUserEdgesCols) UnblockedAt() string  { return colName(c.tableAlias, "UnblockedAt") }  // TIMESTAMP NULL
func (c UserUserEdgesCols) TargetUserID() string { return colName(c.tableAlias, "TargetUserID") } // STRING(36)
func (c UserUserEdgesCols) UnfollowedAt() string { return colName(c.tableAlias, "UnfollowedAt") } // TIMESTAMP NULL
