package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) InsertUser(
	ctx context.Context,
	input repo.InsertUserInput,
) error {
	return InsertUser(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), input)
}

func (rw readWrite) InsertUser(
	ctx context.Context,
	input repo.InsertUserInput,
) error {
	return InsertUser(ctx, rw.tx, input)
}

func InsertUser(
	_ context.Context,
	db BufferWriter,
	input repo.InsertUserInput,
) error {
	table := tables.Get().Users()
	op := spanner.InsertOrUpdateMap(
		table.TableName(),
		map[string]interface{}{
			table.Columns().UserID():        input.UserID,
			table.Columns().LanguageCodes(): input.LanguageCodes,
			table.Columns().CreatedAt():     input.CreatedAt,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
