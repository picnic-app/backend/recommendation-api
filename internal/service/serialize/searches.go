package serialize

import (
	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func Search(s repo.Search) model.Search {
	return model.Search{
		SearchKey: model.SearchKey{
			Kind:     s.Kind,
			UserID:   s.UserID,
			EntityID: s.EntityID,
		},
		CreatedAt: s.CreatedAt,
	}
}
