package service

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
)

func (s *Service) ListenEvents(ctx context.Context, client eventbus.Client) error {
	err := client.Topic(eventbus.TopicID().Profile).Sub(ctx, s.HandleProfileEvent)
	if err != nil {
		return fmt.Errorf("subscribing to %q: %w", eventbus.TopicID().Profile, err)
	}

	err = client.Topic(eventbus.TopicID().Circle).Sub(ctx, s.HandleCircleEvent)
	if err != nil {
		return fmt.Errorf("subscribing to %q: %w", eventbus.TopicID().Circle, err)
	}

	err = client.Topic(eventbus.TopicID().Content).Sub(ctx, s.HandleContentEvent)
	if err != nil {
		return fmt.Errorf("subscribing to %q: %w", eventbus.TopicID().Content, err)
	}

	err = client.Topic(eventbus.TopicID().Classification).Sub(ctx, s.HandleClassificationEvent)
	if err != nil {
		return fmt.Errorf("subscribing to %q: %w", eventbus.TopicID().Classification, err)
	}

	err = client.Topic(eventbus.TopicID().Admin).Sub(ctx, s.HandleAdminEvent)
	if err != nil {
		return fmt.Errorf("subscribing to %q: %w", eventbus.TopicID().Admin, err)
	}

	return nil
}

func HandlePayload[M any](ctx context.Context, evt *event.Event, f func(ctx context.Context, payload M) error) error {
	b, err := json.Marshal(evt.Payload)
	if err != nil {
		return errors.Wrap(err, "marshalling to json")
	}

	var payload M
	if err = json.Unmarshal(b, &payload); err != nil {
		return errors.Wrap(err, "unmarshalling from json")
	}

	return f(ctx, payload)
}

func IgnoreErrors(handler func(context.Context, *event.Event) error) func(ctx context.Context, evt *event.Event) error {
	return func(ctx context.Context, evt *event.Event) error {
		err := handler(ctx, evt)
		if status.Code(err) == codes.InvalidArgument {
			logger.ErrorKV(ctx, "event can't be handled", zap.Error(err))
			return nil
		}

		return err
	}
}

func (s *Service) HandleProfileEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "profile-api")

	switch evt.EvtType {
	case event.UserFollowed:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserFollowedPayload,
		) error {
			var followedAt time.Time
			var err error

			if p.FollowedAt != "" {
				followedAt, err = time.Parse(time.RFC3339Nano, p.FollowedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing followed at time when handling user followed",
						zap.Error(err),
						zap.String("followed_at", p.FollowedAt),
					)
				}
			}

			if p.FollowedAt == "" || followedAt.IsZero() {
				followedAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.FollowUser(ctx, p.UserID, p.FollowedUserID, followedAt); err != nil {
				return fmt.Errorf("failed following user when handling user followed: %w", err)
			}

			return nil
		})
	case event.UserUnfollowed:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserUnfollowedPayload,
		) error {
			var unfollowedAt time.Time
			var err error

			if p.UnfollowedAt != "" {
				unfollowedAt, err = time.Parse(time.RFC3339Nano, p.UnfollowedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing unfollowed at time when handling user unfollowed",
						zap.Error(err),
						zap.String("unfollowed_at", p.UnfollowedAt),
					)
				}
			}

			if p.UnfollowedAt == "" || unfollowedAt.IsZero() {
				unfollowedAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.UnfollowUser(ctx, p.UserID, p.UnfollowedUserID, unfollowedAt); err != nil {
				return fmt.Errorf("failed unfollowing user when handling user unfollowed: %w", err)
			}

			return nil
		})
	case event.UserBlocked:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserBlockedPayload,
		) error {
			var blockedAt time.Time
			var err error

			if p.BlockedAt != "" {
				blockedAt, err = time.Parse(time.RFC3339Nano, p.BlockedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing blocked at time when handling user blocked",
						zap.Error(err),
						zap.String("blocked_at", p.BlockedAt),
					)
				}
			}

			if p.BlockedAt == "" || blockedAt.IsZero() {
				blockedAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.BlockUser(ctx, p.UserID, p.BlockedUserID, blockedAt); err != nil {
				return fmt.Errorf("failed blocking user when handling user blocked: %w", err)
			}

			return nil
		})
	case event.UserUnblocked:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserUnblockedPayload,
		) error {
			var unblockedAt time.Time
			var err error

			if p.UnblockedAt != "" {
				unblockedAt, err = time.Parse(time.RFC3339Nano, p.UnblockedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing unblocked at time when handling user unblocked",
						zap.Error(err),
						zap.String("unblocked_at", p.UnblockedAt),
					)
				}
			}

			if p.UnblockedAt == "" || unblockedAt.IsZero() {
				unblockedAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.UnblockUser(ctx, p.UserID, p.UnblockedUserID, unblockedAt); err != nil {
				return fmt.Errorf("failed unblocking user when handling user unblocked: %w", err)
			}

			return nil
		})
	case event.TypeUserNew:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.NewUserPayload,
		) error {
			var createdAt time.Time
			var err error

			if p.CreatedAt != "" {
				createdAt, err = time.Parse(time.RFC3339Nano, p.CreatedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing created at time when handling user created",
						zap.Error(err),
						zap.String("created_at", p.CreatedAt),
					)
				}
			}

			if p.CreatedAt == "" || createdAt.IsZero() {
				createdAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.CreateUser(ctx, p.UserID, p.LanguageCodes, createdAt); err != nil {
				return fmt.Errorf("failed to create user when handling user created: %w", err)
			}

			return nil
		})
	case event.UserUpdated:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserUpdatedPayload,
		) error {
			if len(p.LanguageCodes) == 0 {
				return nil
			}

			if err := s.UpdateUserLangCodes(ctx, p.UserID, p.LanguageCodes); err != nil {
				return fmt.Errorf("failed to update user language codes when handling user updated: %w", err)
			}

			return nil
		})
	default:
		return nil
	}
}

func (s *Service) HandleCircleEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "circle-api")

	switch evt.EvtType {
	case event.TypeCircleJoin:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.JoinCirclePayload,
		) error {
			eventTime := time.UnixMicro(int64(evt.Timestamp))
			if eventTime.IsZero() {
				return nil
			}

			var err error
			var joinedAt time.Time
			if p.JoinedAt != "" {
				joinedAt, err = time.Parse(time.RFC3339Nano, p.JoinedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing joined at time when handling circle joined",
						zap.Error(err),
						zap.String("joined_at", p.JoinedAt),
					)
				}
			}

			if p.JoinedAt == "" || joinedAt.IsZero() {
				joinedAt = eventTime
			}

			err = s.JoinUserToCircle(ctx, p.UserID, p.CircleID, joinedAt)
			if err != nil {
				return fmt.Errorf("failed joining user to circle: %w", err)
			}

			return nil
		})
	case event.TypeCircleLeave:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.LeaveCirclePayload,
		) error {
			eventTime := time.UnixMicro(int64(evt.Timestamp))
			if eventTime.IsZero() {
				return nil
			}

			var err error
			var leftAt time.Time
			if p.LeftAt != "" {
				leftAt, err = time.Parse(time.RFC3339Nano, p.LeftAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing left at time when handling circle left",
						zap.Error(err),
						zap.String("left_at", p.LeftAt),
					)
				}
			}

			if p.LeftAt == "" || leftAt.IsZero() {
				leftAt = eventTime
			}

			err = s.LeaveUserFromCircle(ctx, p.UserID, p.CircleID, leftAt)
			if err != nil {
				return fmt.Errorf("failed leaving user from circle: %w", err)
			}

			return nil
		})
	case event.TypeCircleNew:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.CreateCirclePayload,
		) error {
			eventTime := time.UnixMicro(int64(evt.Timestamp))
			if eventTime.IsZero() {
				return nil
			}

			err := s.SaveCircle(ctx, p.CircleID, p.GroupID, eventTime)
			if err != nil {
				return fmt.Errorf("failed saving circle created: %w", err)
			}

			return nil
		})
	case event.TypeCircleDelete:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.DeleteCirclePayload,
		) error {
			eventTime := time.UnixMicro(int64(evt.Timestamp))
			if eventTime.IsZero() {
				return nil
			}

			err := s.DeleteCircle(ctx, p.CircleID, eventTime)
			if err != nil {
				return fmt.Errorf("failed saving circle deleted: %w", err)
			}

			return nil
		})
	case event.CircleGroupChanged:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.CircleGroupChangedPayload,
		) error {
			eventTime := time.UnixMicro(int64(evt.Timestamp))
			if eventTime.IsZero() {
				return nil
			}

			err := s.SaveCircle(ctx, p.CircleID, p.GroupID, eventTime)
			if err != nil {
				return fmt.Errorf("failed saving circle group changed: %w", err)
			}

			return nil
		})
	default:
		return nil
	}
}

func (s *Service) HandleContentEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "content-api")

	switch evt.EvtType {
	case event.TypeRemovePost:
		return HandlePayload(ctx, evt, func(ctx context.Context, p event.RemovePostPayload) error {
			return s.RemovePost(ctx, p.PostID, time.UnixMicro(int64(evt.Timestamp)))
		})
	case event.TypeNewPost:
		return HandlePayload(ctx, evt, func(ctx context.Context, p event.NewPostPayload) error {
			createdAt, err := time.Parse(time.RFC3339Nano, p.CreatedAt)
			if err != nil {
				logger.ErrorKV(ctx, "can't parse createdAt", zap.Error(err))
				createdAt = time.UnixMicro(int64(evt.Timestamp))
			}

			return s.SavePost(
				ctx,
				model.Post{
					ID:           p.PostID,
					UserID:       p.UserID,
					CircleID:     p.CircleID,
					CreatedAt:    createdAt,
					Kind:         int64(p.Type),
					LanguageCode: p.LanguageCode,
					OwnerID:      p.OwnerID,
					OwnerKind:    p.OwnerKind,
				},
			)
		})
	default:
		return nil
	}
}

func (s *Service) HandleClassificationEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "classification-api")

	switch evt.EvtType {
	case event.PostClassified:
		return HandlePayload(ctx, evt, func(ctx context.Context, p event.PostClassifiedPayload) error {
			return s.AddPostClassificationTags(ctx, p.PostID, p.ClassificationTags...)
		})
	default:
		return nil
	}
}

func (s *Service) HandleAdminEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "admin-api")

	switch evt.EvtType {
	case event.UserBanned:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserBannedPayload,
		) error {
			var bannedAt time.Time
			var err error

			if p.BannedAt != "" {
				bannedAt, err = time.Parse(time.RFC3339, p.BannedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing banned at time when handling user banned",
						zap.Error(err),
						zap.String("banned_at", p.BannedAt),
					)
				}
			}

			if p.BannedAt == "" || bannedAt.IsZero() {
				bannedAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.BanUser(ctx, p.UserID, bannedAt); err != nil {
				return fmt.Errorf("failed banning user when handling user banned: %w", err)
			}

			return nil
		})
	case event.UserUnbanned:
		return HandlePayload(ctx, evt, func(
			ctx context.Context,
			p event.UserUnbannedPayload,
		) error {
			var unbannedAt time.Time
			var err error

			if p.UnbannedAt != "" {
				unbannedAt, err = time.Parse(time.RFC3339, p.UnbannedAt)
				if err != nil {
					logger.ErrorKV(
						ctx,
						"Failed parsing unbanned at time when handling user unbanned",
						zap.Error(err),
						zap.String("unbanned_at", p.UnbannedAt),
					)
					return err
				}
			}

			if p.UnbannedAt == "" || unbannedAt.IsZero() {
				unbannedAt = time.UnixMicro(int64(evt.Timestamp))
			}

			if err = s.UnbanUser(ctx, p.UserID, unbannedAt); err != nil {
				return fmt.Errorf("failed unbanning user when handling user unbanned: %w", err)
			}

			return nil
		})
	default:
		return nil
	}
}
