package repo

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/libs/golang/cursor"
)

var ErrNotFound = errors.New("not found")

type ReadActions interface {
	GetPosts(context.Context, ...PostID) ([]Post, error)
	GetSearches(context.Context, Kind, UserID, cursor.Cursor) ([]Search, error)
	// GetUserUserEdge returns the edge from userID to targetUserID if it exists and ErrNotFound otherwise.
	GetUserUserEdge(context.Context, UserUserEdgeKey) (UserUserEdge, error)
	// GetCircle returns the circle with the given circleID if it exists and ErrNotFound otherwise.
	GetCircle(context.Context, CircleKey) (Circle, error)
	// GetUserCircleEdge returns the edge from userID to circleID if it exists and ErrNotFound otherwise.
	GetUserCircleEdge(context.Context, UserCircleEdgeKey) (UserCircleEdge, error)
	// GetUser returns the user with the given userID if it exists and ErrNotFound otherwise.
	GetUser(context.Context, UserKey) (User, error)
	ExistsUser(context.Context, UserKey) (bool, error)
	GetPostClassificationTags(context.Context, PostID) ([]string, error)
}

type WriteActions interface {
	UpsertSearch(context.Context, SearchInput) error
	DeleteSearch(context.Context, SearchKey) error

	InsertUserUserEdge(context.Context, InsertUserUserEdgeInput) error
	PartialUpdateUserUserEdge(context.Context, PartialUpdateUserUserEdgeInput) error

	UpsertCircle(context.Context, Circle) error
	DeleteCircle(context.Context, CircleKey) error

	InsertUserCircleEdge(context.Context, UserCircleEdge) error
	PartialUpdateUserCircleEdge(context.Context, PartialUpdateUserCircleEdgeInput) error

	InsertUser(context.Context, InsertUserInput) error
	PartialUpdateUser(context.Context, PartialUpdateUserInput) error

	InsertPosts(context.Context, ...Post) error
	RemovePost(ctx context.Context, postID string, deletedAt time.Time) error

	UpsertPostClassificationTags(ctx context.Context, postID string, tags ...string) error
}

type ReadWriteActions interface {
	ReadActions
	WriteActions
}

type Repo interface {
	SingleRead() ReadActions
	SingleWrite() WriteActions
	ReadOnlyTx(context.Context, func(ctx context.Context, tx ReadActions) error) error
	ReadWriteTx(context.Context, func(ctx context.Context, tx ReadWriteActions) error) (time.Time, error)
}
