package repo

import "time"

type UserUserEdgeKey struct {
	UserID       UserID
	TargetUserID UserID
}

type UserUserEdge struct {
	UserUserEdgeKey
	FollowedAt   *time.Time
	UnfollowedAt *time.Time
	IsFollowing  bool
	BlockedAt    *time.Time
	UnblockedAt  *time.Time
	IsBlocking   bool
}

type InsertUserUserEdgeInput struct {
	UserUserEdgeKey
	FollowedAt   *time.Time
	UnfollowedAt *time.Time
	BlockedAt    *time.Time
	UnblockedAt  *time.Time
}

type PartialUpdateUserUserEdgeInput struct {
	UserUserEdgeKey
	FollowedAt   *time.Time
	UnfollowedAt *time.Time
	BlockedAt    *time.Time
	UnblockedAt  *time.Time
}
