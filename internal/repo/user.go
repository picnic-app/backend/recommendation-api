package repo

import (
	"time"
)

type UserKey struct {
	UserID UserID
}

type User struct {
	UserKey
	LanguageCodes []LanguageCode
	IsBanned      bool
	BannedAt      *time.Time
	UnbannedAt    *time.Time
	CreatedAt     time.Time
}

type InsertUserInput struct {
	UserKey
	LanguageCodes []LanguageCode
	UnbannedAt    *time.Time
	CreatedAt     time.Time
}

type PartialUpdateUserInput struct {
	UserKey
	LanguageCodes []LanguageCode
	BannedAt      *time.Time
	UnbannedAt    *time.Time
}
