package service

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) CreateUser(
	ctx context.Context,
	userID string,
	languageCodes []string,
	createdAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if len(languageCodes) == 0 {
		return ErrRequiredParam("language codes")
	}

	if createdAt.IsZero() {
		return ErrRequiredParam("created at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exists, err := tx.ExistsUser(ctx, repo.UserKey{
			UserID: userID,
		})
		if err != nil {
			return err
		}

		if exists {
			return nil
		}

		err = tx.InsertUser(ctx, repo.InsertUserInput{
			UserKey: repo.UserKey{
				UserID: userID,
			},
			LanguageCodes: languageCodes,
			CreatedAt:     createdAt,
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
