package service

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) UnbanUser(
	ctx context.Context,
	userID string,
	unbannedAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if unbannedAt.IsZero() {
		return ErrRequiredParam("unbanned at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		user, err := tx.GetUser(ctx, repo.UserKey{
			UserID: userID,
		})
		if err != nil {
			return err
		}

		if !user.IsBanned {
			return nil
		}

		if user.BannedAt != nil && user.BannedAt.Before(unbannedAt) {
			err = tx.PartialUpdateUser(ctx, repo.PartialUpdateUserInput{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				UnbannedAt: &unbannedAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
