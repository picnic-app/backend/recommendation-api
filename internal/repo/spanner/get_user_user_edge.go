package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetUserUserEdge(
	ctx context.Context,
	key repo.UserUserEdgeKey,
) (repo.UserUserEdge, error) {
	return GetUserUserEdge(ctx, r.tx, key)
}

func (rw readWrite) GetUserUserEdge(
	ctx context.Context,
	key repo.UserUserEdgeKey,
) (repo.UserUserEdge, error) {
	return GetUserUserEdge(ctx, rw.tx, key)
}

func GetUserUserEdge(
	ctx context.Context,
	db RowReader,
	key repo.UserUserEdgeKey,
) (repo.UserUserEdge, error) {
	table := tables.Get().UserUserEdges()
	e, err := GetByKey[repo.UserUserEdge](ctx, db, table, spanner.Key{key.UserID, key.TargetUserID})
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return repo.UserUserEdge{}, repo.ErrNotFound
		}

		return repo.UserUserEdge{}, err
	}

	return e, nil
}
