package model

const (
	KindApp = iota + 1
	KindChat
	KindPost
	KindCircle
	KindProfile
)
