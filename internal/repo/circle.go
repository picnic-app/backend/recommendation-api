package repo

import "time"

type CircleKey struct {
	ID CircleID
}

type Circle struct {
	CircleKey
	GroupID       GroupID
	LastUpdatedAt *time.Time
}
