package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetUser(ctx context.Context, key repo.UserKey) (repo.User, error) {
	return GetUser(ctx, r.tx, key)
}

func (rw readWrite) GetUser(ctx context.Context, key repo.UserKey) (repo.User, error) {
	return GetUser(ctx, rw.tx, key)
}

func GetUser(ctx context.Context, db RowReader, key repo.UserKey) (repo.User, error) {
	table := tables.Get().Users()
	u, err := GetByKey[repo.User](ctx, db, table, spanner.Key{key.UserID})
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return repo.User{}, repo.ErrNotFound
		}

		return repo.User{}, err
	}

	return u, nil
}
