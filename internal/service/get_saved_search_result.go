package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/cursor"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/util/is"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/util/slice"
)

func (s *Service) GetSavedSearchResult(
	ctx context.Context,
	kind int64,
	c model.Cursor,
) ([]*model.Search, cursor.Page, error) {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, nil, err
	}

	if err = s.ValidateKind(ctx, kind); err != nil {
		return nil, nil, err
	}

	p := c.Params()
	if p.Limit == 0 {
		p.Limit = 5
	}

	cur, err := cursor.FromParams(model.Search{}, p)
	if err != nil {
		return nil, nil, err
	}

	searches, err := s.repo.SingleRead().GetSearches(ctx, kind, userID, cur)
	if err != nil {
		return nil, nil, err
	}

	r, page, err := cursor.GetResult(cur, slice.ConvertPointers(serialize.Search, searches...))
	return r, page, err
}

func (s *Service) ValidateKind(_ context.Context, kind int64) error {
	if is.AnyOf(kind, model.KindApp, model.KindChat, model.KindCircle, model.KindPost, model.KindProfile) {
		return nil
	}

	return ErrUnsupported("kind", kind)
}

func (s *Service) ValidateEntityID(_ context.Context, kind int64, entityID string) error {
	if entityID == "" {
		return ErrRequiredParam("entityID")
	}

	return nil
}
