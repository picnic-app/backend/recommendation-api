package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) ExistsUser(
	ctx context.Context,
	key repo.UserKey,
) (bool, error) {
	return ExistsUser(ctx, r.tx, key)
}

func (rw readWrite) ExistsUser(
	ctx context.Context,
	key repo.UserKey,
) (bool, error) {
	return ExistsUser(ctx, rw.tx, key)
}

func ExistsUser(
	ctx context.Context,
	db RowReader,
	key repo.UserKey,
) (bool, error) {
	return ExistsKey(ctx, db, tables.Get().Users(), spanner.Key{key.UserID})
}
