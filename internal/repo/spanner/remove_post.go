package spanner

import (
	"context"
	"time"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) RemovePost(ctx context.Context, postID string, deletedAt time.Time) error {
	return RemovePost(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), postID, deletedAt)
}

func (rw readWrite) RemovePost(ctx context.Context, postID string, deletedAt time.Time) error {
	return RemovePost(ctx, rw.tx, postID, deletedAt)
}

func RemovePost(_ context.Context, db BufferWriter, postID string, deletedAt time.Time) error {
	table := tables.Get().Posts()

	op := spanner.Update(
		table.TableName(),
		[]string{table.Columns().ID(), table.Columns().DeletedAt()},
		[]interface{}{postID, deletedAt},
	)

	return db.BufferWrite([]*spanner.Mutation{op})
}
