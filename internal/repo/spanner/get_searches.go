package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/Masterminds/squirrel"
	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/libs/golang/cursor"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetSearches(
	ctx context.Context,
	kind int64,
	userID string,
	c cursor.Cursor,
) ([]repo.Search, error) {
	return GetSearches(ctx, r.tx, kind, userID, c)
}

func (rw readWrite) GetSearches(
	ctx context.Context,
	kind int64,
	userID string,
	c cursor.Cursor,
) ([]repo.Search, error) {
	return GetSearches(ctx, rw.tx, kind, userID, c)
}

func GetSearches(
	ctx context.Context,
	db Queryer,
	kind int64,
	userID string,
	c cursor.Cursor,
) ([]repo.Search, error) {
	table := tables.Get().Searches()

	q, args, err := squirrel.
		Select(table.AllColumnNames()...).
		From(table.TableName()).
		Where(squirrel.Eq{table.Columns().Kind(): kind, table.Columns().UserID(): userID}).
		PlaceholderFormat(squirrel.AtP).ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	params := ArgsToParams(args)
	if c != nil {
		q, params, err = cursor.GetBuilder(c, cursor.Spanner).
			WithSQL(q).
			WithParams(params).
			ToSQL()
		if err != nil {
			return nil, errors.WithStack(err)
		}
	}

	stmt := spanner.Statement{SQL: q, Params: params}
	return GetResults[repo.Search](db.Query(ctx, stmt))
}
