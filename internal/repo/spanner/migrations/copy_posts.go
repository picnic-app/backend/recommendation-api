package migrations

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	spannerRepo "gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/util/slice"
)

func (r *Manager) RunCopyPostsMigration(ctx context.Context) {
	r.run(ctx, "copy posts", 1, r.CopyPosts)
}

func (r *Manager) CopyPosts(ctx context.Context) error {
	for {
		posts, err := r.GetTempPosts(ctx, 2000)
		if err != nil {
			return err
		}

		if len(posts) == 0 {
			return nil
		}

		postIDs := slice.Convert(func(post repo.Post) string { return post.ID }, posts...)
		alreadyExist, err := r.GetExistentPostIDs(ctx, postIDs...)
		if err != nil {
			return err
		}

		set := make(map[repo.PostID]struct{}, len(alreadyExist))
		for _, postID := range alreadyExist {
			set[postID] = struct{}{}
		}

		posts = slice.Filter(posts, func(post repo.Post) bool {
			_, exist := set[post.ID]
			return !exist
		})

		err = r.repo.SingleWrite().InsertPosts(ctx, posts...)
		if err != nil {
			return err
		}

		err = r.DeletePosts(ctx, postIDs...)
		if err != nil {
			return err
		}
	}
}

func (r *Manager) GetTempPosts(ctx context.Context, limit int) ([]repo.Post, error) {
	table := tables.Get().TempPosts()

	opts := spanner.ReadOptions{
		Limit:      limit,
		RequestTag: "GetTempPosts",
	}
	iter := r.db.Single().ReadWithOptions(
		ctx,
		table.TableName(),
		spanner.AllKeys(),
		tables.Get().Posts().AllColumnNames(),
		&opts,
	)
	return spannerRepo.GetResults[repo.Post](iter)
}

func (r *Manager) GetExistentPostIDs(ctx context.Context, postIDs ...string) ([]string, error) {
	table := tables.Get().Posts()

	keys := make([]spanner.Key, len(postIDs))
	for i, postID := range postIDs {
		keys[i] = spanner.Key{postID}
	}

	iter := r.db.Single().Read(
		ctx,
		table.TableName(),
		spanner.KeySetFromKeys(keys...),
		[]string{table.Columns().ID()},
	)
	return spannerRepo.GetResults[string](iter)
}

func (r *Manager) DeletePosts(ctx context.Context, postIDs ...string) error {
	table := tables.Get().TempPosts()

	keys := make([]spanner.Key, len(postIDs))
	for i, postID := range postIDs {
		keys[i] = spanner.Key{postID}
	}

	op := spanner.Delete(table.TableName(), spanner.KeySetFromKeys(keys...))
	_, err := r.db.Apply(ctx, []*spanner.Mutation{op}, spanner.ApplyAtLeastOnce())
	return err
}
