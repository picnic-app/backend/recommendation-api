CREATE TABLE Posts
(
    ID           STRING(36) NOT NULL,
    Kind         INT64     NOT NULL,
    CircleID     STRING(36) NOT NULL,
    UserID       STRING(36) NOT NULL,
    LanguageCode STRING(2) NOT NULL,
    CreatedAt    TIMESTAMP NOT NULL,
    DeletedAt    TIMESTAMP,
    OwnerID      STRING(36),
    OwnerKind    INT64     NOT NULL DEFAULT (0),
) PRIMARY KEY (ID);

CREATE TABLE PostClassificationTags
(
    PostID            STRING(36) NOT NULL,
    ClassificationTag STRING(100) NOT NULL,
) PRIMARY KEY (PostID, ClassificationTag);

CREATE TABLE DataMigrations
(
    Label                 STRING(100) NOT NULL,
    Run                   INT64 NOT NULL,
    InstanceId            STRING(36),
    InstanceLockExpiresAt TIMESTAMP,
    CompletedAt           TIMESTAMP,
) PRIMARY KEY (Label, Run);

CREATE TABLE TempPosts
(
    ID              STRING(36) NOT NULL,
    Title           STRING(256) NOT NULL,
    Kind            INT64     NOT NULL,
    Content         STRING(10000),
    MoreContent     STRING(10000),
    TextColor       INT64,
    ContentUrl      STRING(1024),
    ThumbnailUrl    STRING(1024),
    Duration        INT64,
    SoundID         STRING(36),
    CircleID        STRING(36) NOT NULL,
    UserID          STRING(36) NOT NULL,
    LanguageCode    STRING(2) NOT NULL,
    CreatedAt       TIMESTAMP NOT NULL OPTIONS ( allow_commit_timestamp = true ),
    DeletedAt       TIMESTAMP OPTIONS ( allow_commit_timestamp = true ),
    AttachmentID    STRING(36),
    Migration       INT64              DEFAULT (0),
    UserMentions    ARRAY <STRING(36)>,
    CircleMentions  ARRAY <STRING(36)>,
    ContactMentions ARRAY <STRING(36)>,
    MigrationStatus INT64              DEFAULT (0),
    ShortID         STRING(32),
    AppMentions     ARRAY <STRING(36)>,
    OwnerID         STRING(36),
    OwnerKind       INT64     NOT NULL DEFAULT (0),
) PRIMARY KEY (ID);
