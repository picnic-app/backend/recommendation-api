package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) PartialUpdateUser(
	ctx context.Context,
	input repo.PartialUpdateUserInput,
) error {
	return PartialUpdateUser(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), input)
}

func (rw readWrite) PartialUpdateUser(
	ctx context.Context,
	input repo.PartialUpdateUserInput,
) error {
	return PartialUpdateUser(ctx, rw.tx, input)
}

func PartialUpdateUser(
	_ context.Context,
	db BufferWriter,
	input repo.PartialUpdateUserInput,
) error {
	table := tables.Get().Users()

	hasUpdate := false
	kv := map[string]interface{}{
		table.Columns().UserID(): input.UserID,
	}

	if len(input.LanguageCodes) > 0 {
		kv[table.Columns().LanguageCodes()] = input.LanguageCodes
		hasUpdate = true
	}

	if input.BannedAt != nil && input.UnbannedAt != nil {
		return errors.New("cannot set both banned_at and unbanned_at")
	}

	if input.BannedAt != nil {
		kv[table.Columns().BannedAt()] = *input.BannedAt
		hasUpdate = true
	}

	if input.UnbannedAt != nil {
		kv[table.Columns().UnbannedAt()] = *input.UnbannedAt
		hasUpdate = true
	}

	if !hasUpdate {
		return nil
	}

	return db.BufferWrite([]*spanner.Mutation{spanner.UpdateMap(
		table.TableName(),
		kv,
	)})
}
