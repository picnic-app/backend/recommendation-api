package is

func AnyOf[T comparable](a T, b ...T) bool {
	for _, b := range b {
		if a == b {
			return true
		}
	}
	return false
}

func AnyOfCond[T any](cond func(T) bool, args ...T) bool {
	for _, arg := range args {
		if cond(arg) {
			return true
		}
	}
	return false
}
