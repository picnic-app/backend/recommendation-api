package test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
)

func TestSaveSearchResult(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID, chatID := uuid.NewString(), uuid.NewString()

	ctx := auth.AddUserIDToCtx(context.Background(), userID)

	_, err := container.controller.SaveSearchResult(
		ctx,
		&v1.SaveSearchResultRequest{
			Kind:      v1.Kind_KIND_CHAT,
			EntityKey: &v1.EntityKey{Id: chatID},
		},
	)
	require.NoError(t, err)
}

func TestSaveSearchResult_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	ctx := auth.AddUserIDToCtx(context.Background(), uuid.NewString())

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.SaveSearchResultRequest
		code codes.Code
	}{
		{
			name: "unauthenticated",
			ctx:  context.Background(),
			req: &v1.SaveSearchResultRequest{
				Kind:      v1.Kind_KIND_PROFILE,
				EntityKey: &v1.EntityKey{Id: uuid.NewString()},
			},
			code: codes.Unauthenticated,
		},
		{
			name: "invalid kind",
			ctx:  ctx,
			req: &v1.SaveSearchResultRequest{
				Kind:      v1.Kind_KIND_UNSPECIFIED,
				EntityKey: &v1.EntityKey{Id: uuid.NewString()},
			},
			code: codes.InvalidArgument,
		},
		{
			name: "invalid id",
			ctx:  ctx,
			req: &v1.SaveSearchResultRequest{
				Kind:      v1.Kind_KIND_PROFILE,
				EntityKey: &v1.EntityKey{Id: ""},
			},
			code: codes.InvalidArgument,
		},
		{
			name: "ok",
			ctx:  ctx,
			req: &v1.SaveSearchResultRequest{
				Kind:      v1.Kind_KIND_POST,
				EntityKey: &v1.EntityKey{Id: uuid.NewString()},
			},
			code: codes.OK,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.SaveSearchResult(test.ctx, test.req)
			require.Equal(t, test.code, status.Code(err))
		})
	}
}
