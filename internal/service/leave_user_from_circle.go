package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) LeaveUserFromCircle(
	ctx context.Context,
	userID string,
	circleID string,
	leftAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if circleID == "" {
		return ErrRequiredParam("circle id")
	}

	if leftAt.IsZero() {
		return ErrRequiredParam("left at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exits := true
		edge, err := tx.GetUserCircleEdge(ctx, repo.UserCircleEdgeKey{
			UserID:   userID,
			CircleID: circleID,
		})
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				exits = false
			} else {
				return err
			}
		}

		if exits {
			if !edge.IsMember {
				return nil
			}

			if edge.JoinedAt == nil || edge.JoinedAt != nil && edge.JoinedAt.Before(leftAt) {
				err = tx.PartialUpdateUserCircleEdge(ctx, repo.PartialUpdateUserCircleEdgeInput{
					UserCircleEdgeKey: repo.UserCircleEdgeKey{
						UserID:   userID,
						CircleID: circleID,
					},
					LeftAt: &leftAt,
				})
				if err != nil {
					return err
				}
			}
		} else {
			err = tx.InsertUserCircleEdge(ctx, repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				LeftAt:    &leftAt,
				CreatedAt: leftAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
