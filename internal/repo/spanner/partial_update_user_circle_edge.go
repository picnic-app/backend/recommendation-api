package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) PartialUpdateUserCircleEdge(
	ctx context.Context,
	input repo.PartialUpdateUserCircleEdgeInput,
) error {
	return PartialUpdateUserCircleEdge(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), input)
}

func (rw readWrite) PartialUpdateUserCircleEdge(
	ctx context.Context,
	input repo.PartialUpdateUserCircleEdgeInput,
) error {
	return PartialUpdateUserCircleEdge(ctx, rw.tx, input)
}

func PartialUpdateUserCircleEdge(
	_ context.Context,
	db BufferWriter,
	input repo.PartialUpdateUserCircleEdgeInput,
) error {
	table := tables.Get().UserCircleEdges()

	hasUpdate := false
	kv := map[string]interface{}{
		table.Columns().UserID():   input.UserCircleEdgeKey.UserID,
		table.Columns().CircleID(): input.UserCircleEdgeKey.CircleID,
	}

	if input.JoinedAt != nil && input.LeftAt != nil {
		return errors.New("cannot set both joined_at and left_at")
	}

	if input.JoinedAt != nil {
		kv[table.Columns().JoinedAt()] = *input.JoinedAt
		hasUpdate = true
	}

	if input.LeftAt != nil {
		kv[table.Columns().LeftAt()] = *input.LeftAt
		hasUpdate = true
	}

	if !hasUpdate {
		return nil
	}

	return db.BufferWrite([]*spanner.Mutation{spanner.UpdateMap(
		table.TableName(),
		kv,
	)})
}
