package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) InsertPosts(ctx context.Context, posts ...repo.Post) error {
	return InsertPosts(ctx, w.bufferWriter(ctx), posts...)
}

func (rw readWrite) InsertPosts(ctx context.Context, posts ...repo.Post) error {
	return InsertPosts(ctx, rw.tx, posts...)
}

func InsertPosts(_ context.Context, db BufferWriter, posts ...repo.Post) error {
	if len(posts) == 0 {
		return nil
	}

	table := tables.Get().Posts()
	cols := table.Columns()

	ops := make([]*spanner.Mutation, len(posts))
	for i, post := range posts {
		ops[i] = spanner.InsertMap(
			table.TableName(),
			map[string]interface{}{
				cols.ID():           post.ID,
				cols.UserID():       post.UserID,
				cols.CircleID():     post.CircleID,
				cols.CreatedAt():    post.CreatedAt,
				cols.Kind():         post.Kind,
				cols.LanguageCode(): post.LanguageCode,
				cols.OwnerID():      post.OwnerID,
				cols.OwnerKind():    post.OwnerKind,
				cols.DeletedAt():    post.DeletedAt,
			},
		)
	}

	return db.BufferWrite(ops)
}
