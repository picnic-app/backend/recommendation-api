package serialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
)

func Kind(k int64) v1.Kind {
	switch k {
	case model.KindApp:
		return v1.Kind_KIND_APP
	case model.KindChat:
		return v1.Kind_KIND_CHAT
	case model.KindPost:
		return v1.Kind_KIND_POST
	case model.KindCircle:
		return v1.Kind_KIND_CIRCLE
	case model.KindProfile:
		return v1.Kind_KIND_PROFILE
	}
	return v1.Kind_KIND_UNSPECIFIED
}
