package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func TestUserUserEdge(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	user1ID := uuid.NewString()
	user2ID := uuid.NewString()
	user3ID := uuid.NewString()
	user4ID := uuid.NewString()
	user5ID := uuid.NewString()
	user6ID := uuid.NewString()
	targetUserID := uuid.NewString()
	now := time.UnixMicro(time.Now().UTC().UnixMicro())
	nowPlus5Minute := now.Add(5 * time.Minute)
	nowPlus10Minute := now.Add(10 * time.Minute)
	nowPlus15Minute := now.Add(15 * time.Minute)
	nowPlus20Minute := now.Add(20 * time.Minute)

	testCases := []struct {
		name        string
		event       event.Event
		expected    repo.UserUserEdge
		expectedErr error
	}{
		{
			name: "user1_followed_another_user_initially",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserFollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserFollowedPayload{
					UserID:         user1ID,
					FollowedUserID: targetUserID,
					FollowedAt:     nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user1ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: nil,
				IsFollowing:  true,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user1_unfollowed_another_user_ignored_when_unfollowed_at_is_before_followed_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnfollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserUnfollowedPayload{
					UserID:           user1ID,
					UnfollowedUserID: targetUserID,
					UnfollowedAt:     now.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user1ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: nil,
				IsFollowing:  true,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user1_unfollowed_another_user",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnfollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserUnfollowedPayload{
					UserID:           user1ID,
					UnfollowedUserID: targetUserID,
					UnfollowedAt:     nowPlus10Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user1ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user1_followed_another_user_ignored_when_followed_at_is_after_unfollowed_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserFollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserFollowedPayload{
					UserID:         user1ID,
					FollowedUserID: targetUserID,
					FollowedAt:     nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user1ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user1_followed_another_user_again",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserFollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserFollowedPayload{
					UserID:         user1ID,
					FollowedUserID: targetUserID,
					FollowedAt:     nowPlus15Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user1ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus15Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  true,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user2_unfollowed_another_user_initially",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnfollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserUnfollowedPayload{
					UserID:           user2ID,
					UnfollowedUserID: targetUserID,
					UnfollowedAt:     nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user2ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: &nowPlus5Minute,
				IsFollowing:  false,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user3_blocked_another_user_initially",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBlocked,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserBlockedPayload{
					UserID:        user3ID,
					BlockedUserID: targetUserID,
					BlockedAt:     nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user3ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: nil,
				IsFollowing:  false,
				BlockedAt:    &nowPlus5Minute,
				UnblockedAt:  nil,
				IsBlocking:   true,
			},
		},
		{
			name: "user3_unblocked_another_user_ignored_when_unblocked_at_is_before_blocked_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnblocked,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserUnblockedPayload{
					UserID:          user3ID,
					UnblockedUserID: targetUserID,
					UnblockedAt:     now.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user3ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: nil,
				IsFollowing:  false,
				BlockedAt:    &nowPlus5Minute,
				UnblockedAt:  nil,
				IsBlocking:   true,
			},
		},
		{
			name: "user3_unblocked_another_user",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnblocked,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserUnblockedPayload{
					UserID:          user3ID,
					UnblockedUserID: targetUserID,
					UnblockedAt:     nowPlus10Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user3ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: nil,
				IsFollowing:  false,
				BlockedAt:    &nowPlus5Minute,
				UnblockedAt:  &nowPlus10Minute,
				IsBlocking:   false,
			},
		},
		{
			name: "user3_blocked_another_user_ignored_when_blocked_at_is_before_unblocked_at",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBlocked,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserBlockedPayload{
					UserID:        user3ID,
					BlockedUserID: targetUserID,
					BlockedAt:     nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user3ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: nil,
				IsFollowing:  false,
				BlockedAt:    &nowPlus5Minute,
				UnblockedAt:  &nowPlus10Minute,
				IsBlocking:   false,
			},
		},
		{
			name: "user3_blocked_another_user_again",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBlocked,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.UserBlockedPayload{
					UserID:        user3ID,
					BlockedUserID: targetUserID,
					BlockedAt:     nowPlus15Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user3ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: nil,
				IsFollowing:  false,
				BlockedAt:    &nowPlus15Minute,
				UnblockedAt:  &nowPlus10Minute,
				IsBlocking:   true,
			},
		},
		{
			name: "user4_unblocked_another_user_initially",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnblocked,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserUnblockedPayload{
					UserID:          user4ID,
					UnblockedUserID: targetUserID,
					UnblockedAt:     nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user4ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   nil,
				UnfollowedAt: nil,
				IsFollowing:  false,
				BlockedAt:    nil,
				UnblockedAt:  &nowPlus5Minute,
				IsBlocking:   false,
			},
		},
		{
			name: "user5_followed_when_followedAt_is_empty",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserFollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserFollowedPayload{
					UserID:         user5ID,
					FollowedUserID: targetUserID,
					FollowedAt:     "",
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user5ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: nil,
				IsFollowing:  true,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user5_unfollowed_when_unfollowedAt_is_empty",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnfollowed,
				Timestamp: uint64(nowPlus10Minute.UnixMicro()),
				Payload: event.UserUnfollowedPayload{
					UserID:           user5ID,
					UnfollowedUserID: targetUserID,
					UnfollowedAt:     "",
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user5ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user5_blocked_when_unfollowedAt_is_empty",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBlocked,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.UserBlockedPayload{
					UserID:        user5ID,
					BlockedUserID: targetUserID,
					BlockedAt:     "",
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user5ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    &nowPlus15Minute,
				UnblockedAt:  nil,
				IsBlocking:   true,
			},
		},
		{
			name: "user5_unblocked_when_unfollowedAt_is_empty",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnblocked,
				Timestamp: uint64(nowPlus20Minute.UnixMicro()),
				Payload: event.UserUnblockedPayload{
					UserID:          user5ID,
					UnblockedUserID: targetUserID,
					UnblockedAt:     "",
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user5ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    &nowPlus15Minute,
				UnblockedAt:  &nowPlus20Minute,
				IsBlocking:   false,
			},
		},
		{
			name: "user6_followed_when_followedAt_is_zero",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserFollowed,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.UserFollowedPayload{
					UserID:         user6ID,
					FollowedUserID: targetUserID,
					FollowedAt:     time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user6ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: nil,
				IsFollowing:  true,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user6_unfollowed_when_unfollowedAt_is_zero",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnfollowed,
				Timestamp: uint64(nowPlus10Minute.UnixMicro()),
				Payload: event.UserUnfollowedPayload{
					UserID:           user6ID,
					UnfollowedUserID: targetUserID,
					UnfollowedAt:     time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user6ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    nil,
				UnblockedAt:  nil,
				IsBlocking:   false,
			},
		},
		{
			name: "user6_blocked_when_unfollowedAt_is_zero",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBlocked,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.UserBlockedPayload{
					UserID:        user6ID,
					BlockedUserID: targetUserID,
					BlockedAt:     time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user6ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    &nowPlus15Minute,
				UnblockedAt:  nil,
				IsBlocking:   true,
			},
		},
		{
			name: "user6_unblocked_when_unfollowedAt_is_zero",
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnblocked,
				Timestamp: uint64(nowPlus20Minute.UnixMicro()),
				Payload: event.UserUnblockedPayload{
					UserID:          user6ID,
					UnblockedUserID: targetUserID,
					UnblockedAt:     time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.UserUserEdge{
				UserUserEdgeKey: repo.UserUserEdgeKey{
					UserID:       user6ID,
					TargetUserID: targetUserID,
				},
				FollowedAt:   &nowPlus5Minute,
				UnfollowedAt: &nowPlus10Minute,
				IsFollowing:  false,
				BlockedAt:    &nowPlus15Minute,
				UnblockedAt:  &nowPlus20Minute,
				IsBlocking:   false,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := container.bus.Topic(eventbus.TopicID().Profile).Pub(context.Background(), tc.event)
			require.NoError(t, err)

			var edge repo.UserUserEdge
			Eventually(t, func() bool {
				edge, err = container.spannerRepo.SingleRead().GetUserUserEdge(
					context.Background(),
					tc.expected.UserUserEdgeKey,
				)

				if tc.expectedErr != nil {
					t.Logf("Expected error: '%v', got: '%v'", tc.expectedErr, err)
					return errors.Is(err, tc.expectedErr)
				}

				if err != nil {
					t.Logf("Unexpected error: '%v'", err)
					return false
				}

				if !cmp.Equal(tc.expected, edge) {
					t.Logf("Expected:\n %v\n, \ngot:\n %v\n Diff: %s", tc.expected, edge, cmp.Diff(tc.expected, edge))
					return false
				}

				return true
			})
		})
	}
}
