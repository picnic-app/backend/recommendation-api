package test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
)

func TestUnsaveSearchResult(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID, circleID := uuid.NewString(), uuid.NewString()

	ctx := auth.AddUserIDToCtx(context.Background(), userID)

	const kind = v1.Kind_KIND_CIRCLE

	_, err := container.controller.SaveSearchResult(
		ctx,
		&v1.SaveSearchResultRequest{
			Kind:      kind,
			EntityKey: &v1.EntityKey{Id: circleID},
		},
	)
	require.NoError(t, err)

	resp, err := container.controller.GetSavedSearchResults(ctx, &v1.GetSavedSearchResultsRequest{Kind: kind})
	require.NoError(t, err)
	require.Len(t, resp.GetResults(), 1)
	require.Equal(t, kind, resp.GetResults()[0].GetKind())
	require.Equal(t, circleID, resp.GetResults()[0].GetEntityKey().GetId())

	_, err = container.controller.UnsaveSearchResult(
		ctx,
		&v1.UnsaveSearchResultRequest{
			Kind:      kind,
			EntityKey: &v1.EntityKey{Id: circleID},
		},
	)
	require.NoError(t, err)

	resp, err = container.controller.GetSavedSearchResults(ctx, &v1.GetSavedSearchResultsRequest{Kind: kind})
	require.NoError(t, err)
	require.Len(t, resp.GetResults(), 0)
}

func TestUnsaveSearchResult_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	ctx := auth.AddUserIDToCtx(context.Background(), uuid.NewString())

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.UnsaveSearchResultRequest
		code codes.Code
	}{
		{
			name: "unauthenticated",
			ctx:  context.Background(),
			req: &v1.UnsaveSearchResultRequest{
				Kind:      v1.Kind_KIND_PROFILE,
				EntityKey: &v1.EntityKey{Id: uuid.NewString()},
			},
			code: codes.Unauthenticated,
		},
		{
			name: "invalid kind",
			ctx:  ctx,
			req: &v1.UnsaveSearchResultRequest{
				Kind:      v1.Kind_KIND_UNSPECIFIED,
				EntityKey: &v1.EntityKey{Id: uuid.NewString()},
			},
			code: codes.InvalidArgument,
		},
		{
			name: "invalid id",
			ctx:  ctx,
			req: &v1.UnsaveSearchResultRequest{
				Kind:      v1.Kind_KIND_PROFILE,
				EntityKey: &v1.EntityKey{Id: ""},
			},
			code: codes.InvalidArgument,
		},
		{
			name: "ok",
			ctx:  ctx,
			req: &v1.UnsaveSearchResultRequest{
				Kind:      v1.Kind_KIND_POST,
				EntityKey: &v1.EntityKey{Id: uuid.NewString()},
			},
			code: codes.OK,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.UnsaveSearchResult(test.ctx, test.req)
			require.Equal(t, test.code, status.Code(err))
		})
	}
}
