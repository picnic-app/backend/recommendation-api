package model

import "time"

type SearchKey struct {
	Kind     int64
	UserID   string
	EntityID string
}

type SearchInput struct {
	SearchKey
}

type Search struct {
	SearchKey
	CreatedAt time.Time `cursor:"offset,desc,default"`
}
