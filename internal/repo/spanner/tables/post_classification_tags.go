// Code generated by tablegen. DO NOT EDIT.
package tables

func (t AllTables) PostClassificationTags() PostClassificationTags { return PostClassificationTags(t) }

type PostClassificationTags struct{ alias string }

func (t PostClassificationTags) TableAlias() string { return t.alias }
func (t PostClassificationTags) name() string       { return "PostClassificationTags" }
func (t PostClassificationTags) TableName() string  { return tableName(t.name(), "", t.alias) }
func (t PostClassificationTags) AllColumnNames() []string {
	c := t.Columns()
	return []string{
		c.PostID(),
		c.ClassificationTag(),
	}
}

func (t PostClassificationTags) Columns() PostClassificationTagsCols {
	return PostClassificationTagsCols{tableAlias: t.alias}
}

type PostClassificationTagsCols struct{ tableAlias string }

func (c PostClassificationTagsCols) PostID() string { return colName(c.tableAlias, "PostID") } // STRING(36)
func (c PostClassificationTagsCols) ClassificationTag() string {
	return colName(c.tableAlias, "ClassificationTag")
} // STRING(100)
