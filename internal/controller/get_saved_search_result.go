package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/controller/deserialize"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/util/slice"
)

func (c Controller) GetSavedSearchResults(
	ctx context.Context,
	req *v1.GetSavedSearchResultsRequest,
) (*v1.GetSavedSearchResultsResponse, error) {
	kind, err := deserialize.Kind(req.GetKind())
	if err != nil {
		return nil, err
	}

	searches, page, err := c.service.GetSavedSearchResult(ctx, kind, deserialize.Cursor(req.GetCursor()))
	if err != nil {
		return nil, err
	}

	resp := &v1.GetSavedSearchResultsResponse{
		PageInfo: serialize.PageInfo(page),
		Results:  slice.Convert(serialize.SearchPointer, searches...),
	}
	return resp, nil
}
