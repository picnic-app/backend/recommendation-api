package serialize

import (
	"google.golang.org/protobuf/types/known/timestamppb"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
)

func Search(s model.Search) *v1.SavedSearchResult {
	return &v1.SavedSearchResult{
		Kind:       Kind(s.Kind),
		EntityKey:  EntityID(s.EntityID),
		SearchedAt: timestamppb.New(s.CreatedAt),
	}
}

func SearchPointer(s *model.Search) *v1.SavedSearchResult {
	if s == nil {
		return nil
	}
	return Search(*s)
}
