package service

import (
	"context"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) SavePost(ctx context.Context, post model.Post) error {
	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		posts, err := tx.GetPosts(ctx, post.ID)
		if err != nil || len(posts) > 0 {
			return err
		}

		return tx.InsertPosts(
			ctx,
			repo.Post{
				ID:           post.ID,
				UserID:       post.UserID,
				CircleID:     post.CircleID,
				CreatedAt:    post.CreatedAt,
				Kind:         post.Kind,
				LanguageCode: post.LanguageCode,
				OwnerID:      post.OwnerID,
				OwnerKind:    post.OwnerKind,
				DeletedAt:    post.DeletedAt,
			},
		)
	})
	return err
}
