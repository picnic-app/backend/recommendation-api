package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/util/to"
)

const (
	profileTopic = "profile"
	adminTopic   = "admin"
)

func TestUser(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	user1ID := uuid.NewString()
	user2ID := uuid.NewString()
	now := time.UnixMicro(time.Now().UTC().UnixMicro())
	nowPlus5Minute := now.Add(5 * time.Minute)
	nowPlus10Minute := now.Add(10 * time.Minute)
	nowPlus15Minute := now.Add(15 * time.Minute)
	nowPlus20Minute := now.Add(20 * time.Minute)

	testCases := []struct {
		name        string
		topic       string
		event       event.Event
		expected    repo.User
		expectedErr error
	}{
		{
			name:  "new_user_created",
			topic: profileTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeUserNew,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.NewUserPayload{
					UserID:        userID,
					LanguageCodes: []string{"en", "nl"},
					CreatedAt:     now.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      false,
				BannedAt:      nil,
				UnbannedAt:    nil,
				CreatedAt:     now,
			},
		},
		{
			name:  "new_user_ignored_when_already_exists",
			topic: profileTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeUserNew,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.NewUserPayload{
					UserID:        userID,
					LanguageCodes: []string{"en"},
					CreatedAt:     now.Add(time.Hour).Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      false,
				BannedAt:      nil,
				UnbannedAt:    nil,
				CreatedAt:     now,
			},
		},
		{
			name:  "users_language_codes_updated",
			topic: profileTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUpdated,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserUpdatedPayload{
					UserID:        userID,
					LanguageCodes: []string{"pl"},
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"pl"},
				IsBanned:      false,
				BannedAt:      nil,
				UnbannedAt:    nil,
				CreatedAt:     now,
			},
		},
		{
			name:  "users_language_codes_update_ignored_when_user_does_not_exist",
			topic: profileTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUpdated,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserUpdatedPayload{
					UserID:        uuid.NewString(),
					LanguageCodes: []string{"pl"},
				},
			},
			expectedErr: repo.ErrNotFound,
		},
		{
			name:  "user_banned",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBanned,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserBannedPayload{
					UserID:   userID,
					BannedAt: nowPlus10Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"pl"},
				IsBanned:      true,
				BannedAt:      to.Ptr(nowPlus10Minute),
				UnbannedAt:    nil,
				CreatedAt:     now,
			},
		},
		{
			name:  "user_unbanned_ignored_when_its_before_banned_at",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnbanned,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserUnbannedPayload{
					UserID:     userID,
					UnbannedAt: nowPlus5Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"pl"},
				IsBanned:      true,
				BannedAt:      to.Ptr(nowPlus10Minute),
				UnbannedAt:    nil,
				CreatedAt:     now,
			},
		},
		{
			name:  "user_unbanned",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnbanned,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserUnbannedPayload{
					UserID:     userID,
					UnbannedAt: nowPlus15Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"pl"},
				IsBanned:      false,
				BannedAt:      to.Ptr(nowPlus10Minute),
				UnbannedAt:    to.Ptr(nowPlus15Minute),
				CreatedAt:     now,
			},
		},
		{
			name:  "user_banned_ignored_when_its_before_unbanned_at",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBanned,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserBannedPayload{
					UserID:   userID,
					BannedAt: nowPlus10Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"pl"},
				IsBanned:      false,
				BannedAt:      to.Ptr(nowPlus10Minute),
				UnbannedAt:    to.Ptr(nowPlus15Minute),
				CreatedAt:     now,
			},
		},
		{
			name:  "user_banned_again",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBanned,
				Timestamp: uint64(time.Now().UnixMicro()),
				Payload: event.UserBannedPayload{
					UserID:   userID,
					BannedAt: nowPlus20Minute.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: userID,
				},
				LanguageCodes: []string{"pl"},
				IsBanned:      true,
				BannedAt:      to.Ptr(nowPlus20Minute),
				UnbannedAt:    to.Ptr(nowPlus15Minute),
				CreatedAt:     now,
			},
		},
		{
			name:  "new_user_created_when_empty_created_at",
			topic: profileTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeUserNew,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.NewUserPayload{
					UserID:        user1ID,
					LanguageCodes: []string{"en", "nl"},
					CreatedAt:     "",
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: user1ID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      false,
				BannedAt:      nil,
				UnbannedAt:    nil,
				CreatedAt:     nowPlus5Minute,
			},
		},
		{
			name:  "user_banned_when_empty_created_at",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBanned,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.UserBannedPayload{
					UserID:   user1ID,
					BannedAt: "",
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: user1ID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      true,
				BannedAt:      to.Ptr(nowPlus15Minute),
				UnbannedAt:    nil,
				CreatedAt:     nowPlus5Minute,
			},
		},
		{
			name:  "user_unbanned_when_empty_created_at",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnbanned,
				Timestamp: uint64(nowPlus20Minute.UnixMicro()),
				Payload: event.UserUnbannedPayload{
					UserID:     user1ID,
					UnbannedAt: "",
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: user1ID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      false,
				BannedAt:      to.Ptr(nowPlus15Minute),
				UnbannedAt:    to.Ptr(nowPlus20Minute),
				CreatedAt:     nowPlus5Minute,
			},
		},
		{
			name:  "new_user_created_when_created_at_is_zero",
			topic: profileTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.TypeUserNew,
				Timestamp: uint64(nowPlus5Minute.UnixMicro()),
				Payload: event.NewUserPayload{
					UserID:        user2ID,
					LanguageCodes: []string{"en", "nl"},
					CreatedAt:     time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: user2ID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      false,
				BannedAt:      nil,
				UnbannedAt:    nil,
				CreatedAt:     nowPlus5Minute,
			},
		},
		{
			name:  "user_banned_when_created_at_is_zero",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserBanned,
				Timestamp: uint64(nowPlus15Minute.UnixMicro()),
				Payload: event.UserBannedPayload{
					UserID:   user2ID,
					BannedAt: time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: user2ID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      true,
				BannedAt:      to.Ptr(nowPlus15Minute),
				UnbannedAt:    nil,
				CreatedAt:     nowPlus5Minute,
			},
		},
		{
			name:  "user_unbanned_when_created_at_is_zero",
			topic: adminTopic,
			event: event.Event{
				EvtID:     uuid.NewString(),
				EvtType:   event.UserUnbanned,
				Timestamp: uint64(nowPlus20Minute.UnixMicro()),
				Payload: event.UserUnbannedPayload{
					UserID:     user2ID,
					UnbannedAt: time.Time{}.Format(time.RFC3339Nano),
				},
			},
			expected: repo.User{
				UserKey: repo.UserKey{
					UserID: user2ID,
				},
				LanguageCodes: []string{"en", "nl"},
				IsBanned:      false,
				BannedAt:      to.Ptr(nowPlus15Minute),
				UnbannedAt:    to.Ptr(nowPlus20Minute),
				CreatedAt:     nowPlus5Minute,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := container.bus.Topic(tc.topic).Pub(context.Background(), tc.event)
			require.NoError(t, err)

			var user repo.User
			Eventually(t, func() bool {
				user, err = container.spannerRepo.SingleRead().GetUser(context.Background(), repo.UserKey{
					UserID: tc.expected.UserID,
				})

				if tc.expectedErr != nil {
					t.Logf("Expected error: '%v', got: '%v'", tc.expectedErr, err)
					return errors.Is(err, tc.expectedErr)
				}

				if err != nil {
					t.Logf("Unexpected error: '%v'", err)
					return false
				}

				if !cmp.Equal(tc.expected, user) {
					t.Logf("Expected:\n %v\n, \ngot:\n %v\n Diff: %s", tc.expected, user, cmp.Diff(tc.expected, user))
					return false
				}

				return true
			})
		})
	}
}
