package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
)

func TestPostSavedAndClassifiedAndRemoved(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	payload := event.NewPostPayload{
		PostID:       uuid.NewString(),
		UserID:       uuid.NewString(),
		CircleID:     uuid.NewString(),
		CreatedAt:    time.Now().Format(time.RFC3339Nano),
		Type:         1,
		LanguageCode: "en",
	}

	evt := event.NewEvent(event.TypeNewPost, payload)

	ctx := context.Background()

	err := container.bus.Topic(eventbus.TopicID().Content).Pub(ctx, evt)
	require.NoError(t, err)

	cond := func() bool {
		posts, err := container.spannerRepo.SingleRead().GetPosts(ctx, payload.PostID)
		require.NoError(t, err)
		return len(posts) > 0
	}
	Eventually(t, cond)

	evt = event.NewEvent(
		event.PostClassified,
		event.PostClassifiedPayload{
			PostID:             payload.PostID,
			PostType:           "text",
			ClassificationTags: []string{"tag1", "tag2"},
		},
	)
	err = container.bus.Topic(eventbus.TopicID().Classification).Pub(ctx, evt)
	require.NoError(t, err)

	cond = func() bool {
		tags, err := container.spannerRepo.SingleRead().GetPostClassificationTags(ctx, payload.PostID)
		require.NoError(t, err)
		return len(tags) > 0
	}
	Eventually(t, cond)

	evt = event.NewEvent(
		event.TypeRemovePost,
		event.RemovePostPayload{
			PostID:   payload.PostID,
			CircleID: payload.CircleID,
			UserID:   payload.UserID,
		},
	)
	err = container.bus.Topic(eventbus.TopicID().Content).Pub(ctx, evt)
	require.NoError(t, err)

	cond = func() bool {
		posts, err := container.spannerRepo.SingleRead().GetPosts(ctx, payload.PostID)
		require.NoError(t, err)
		return posts[0].DeletedAt != nil
	}
	Eventually(t, cond)
}
