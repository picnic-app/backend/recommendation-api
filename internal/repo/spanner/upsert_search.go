package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) UpsertSearch(ctx context.Context, in repo.SearchInput) error {
	return UpsertSearch(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), in)
}

func (rw readWrite) UpsertSearch(ctx context.Context, in repo.SearchInput) error {
	return UpsertSearch(ctx, rw.tx, in)
}

func UpsertSearch(_ context.Context, db BufferWriter, in repo.SearchInput) error {
	table := tables.Get().Searches()
	op := spanner.InsertOrUpdate(
		table.TableName(),
		[]string{
			table.Columns().Kind(),
			table.Columns().UserID(),
			table.Columns().EntityID(),
			table.Columns().CreatedAt(),
		},
		[]interface{}{
			in.Kind,
			in.UserID,
			in.EntityID,
			spanner.CommitTimestamp,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
