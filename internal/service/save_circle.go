package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) SaveCircle(
	ctx context.Context,
	circleID string,
	groupID string,
	lastUpdateAt time.Time,
) error {
	if circleID == "" {
		return ErrRequiredParam("circle id")
	}

	if groupID == "" {
		return ErrRequiredParam("group id")
	}

	if lastUpdateAt.IsZero() {
		return ErrRequiredParam("last update at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		circle, err := tx.GetCircle(ctx, repo.CircleKey{
			ID: circleID,
		})
		if err != nil {
			if !errors.Is(err, repo.ErrNotFound) {
				return err
			}
		}

		if circle.LastUpdatedAt != nil && circle.LastUpdatedAt.After(lastUpdateAt) {
			return nil
		}

		return tx.UpsertCircle(ctx, repo.Circle{
			CircleKey: repo.CircleKey{
				ID: circleID,
			},
			GroupID:       groupID,
			LastUpdatedAt: &lastUpdateAt,
		})
	})
	if err != nil {
		return err
	}

	return nil
}
