package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) JoinUserToCircle(
	ctx context.Context,
	userID string,
	circleID string,
	joinedAt time.Time,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if circleID == "" {
		return ErrRequiredParam("circle id")
	}

	if joinedAt.IsZero() {
		return ErrRequiredParam("joined at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exits := true
		edge, err := tx.GetUserCircleEdge(ctx, repo.UserCircleEdgeKey{
			UserID:   userID,
			CircleID: circleID,
		})
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				exits = false
			} else {
				return err
			}
		}

		if exits {
			if edge.IsMember {
				return nil
			}

			if edge.LeftAt == nil || edge.LeftAt != nil && edge.LeftAt.Before(joinedAt) {
				err = tx.PartialUpdateUserCircleEdge(ctx, repo.PartialUpdateUserCircleEdgeInput{
					UserCircleEdgeKey: repo.UserCircleEdgeKey{
						UserID:   userID,
						CircleID: circleID,
					},
					JoinedAt: &joinedAt,
				})
				if err != nil {
					return err
				}
			}
		} else {
			err = tx.InsertUserCircleEdge(ctx, repo.UserCircleEdge{
				UserCircleEdgeKey: repo.UserCircleEdgeKey{
					UserID:   userID,
					CircleID: circleID,
				},
				JoinedAt:  &joinedAt,
				CreatedAt: joinedAt,
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
