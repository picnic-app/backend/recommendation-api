package service

import (
	"context"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) UpdateUserLangCodes(
	ctx context.Context,
	userID string,
	languagesCodes []string,
) error {
	if userID == "" {
		return ErrRequiredParam("user id")
	}

	if len(languagesCodes) == 0 {
		return ErrRequiredParam("language codes")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exists, err := tx.ExistsUser(ctx, repo.UserKey{
			UserID: userID,
		})
		if err != nil {
			return err
		}

		if !exists {
			// Exit early if user does not exist
			return nil
		}

		err = tx.PartialUpdateUser(ctx, repo.PartialUpdateUserInput{
			UserKey: repo.UserKey{
				UserID: userID,
			},
			LanguageCodes: languagesCodes,
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
