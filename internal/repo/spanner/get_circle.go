package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetCircle(ctx context.Context, key repo.CircleKey) (repo.Circle, error) {
	return GetCircle(ctx, r.tx, key)
}

func (rw readWrite) GetCircle(ctx context.Context, key repo.CircleKey) (repo.Circle, error) {
	return GetCircle(ctx, rw.tx, key)
}

func GetCircle(ctx context.Context, db RowReader, key repo.CircleKey) (repo.Circle, error) {
	table := tables.Get().Circles()
	c, err := GetByKey[repo.Circle](ctx, db, table, spanner.Key{key.ID})
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return repo.Circle{}, repo.ErrNotFound
		}

		return repo.Circle{}, err
	}

	return c, nil
}
