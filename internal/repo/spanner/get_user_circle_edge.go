package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetUserCircleEdge(ctx context.Context, key repo.UserCircleEdgeKey) (repo.UserCircleEdge, error) {
	return GetUserCircleEdge(ctx, r.tx, key)
}

func (rw readWrite) GetUserCircleEdge(ctx context.Context, key repo.UserCircleEdgeKey) (repo.UserCircleEdge, error) {
	return GetUserCircleEdge(ctx, rw.tx, key)
}

func GetUserCircleEdge(ctx context.Context, db RowReader, key repo.UserCircleEdgeKey) (repo.UserCircleEdge, error) {
	table := tables.Get().UserCircleEdges()
	e, err := GetByKey[repo.UserCircleEdge](ctx, db, table, spanner.Key{key.UserID, key.CircleID})
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return repo.UserCircleEdge{}, repo.ErrNotFound
		}

		return repo.UserCircleEdge{}, err
	}

	return e, nil
}
