package model

import "time"

type Post struct {
	ID           string
	UserID       string
	CircleID     string
	CreatedAt    time.Time
	Kind         int64
	LanguageCode string
	OwnerID      *string
	OwnerKind    int64
	DeletedAt    *time.Time
}
