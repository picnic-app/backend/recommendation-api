module "workload-identity" {
  source  = "gitlab.com/picnic-app/workload-identity/google"
  version = "~> 1.0"

  service_name = var.service_name
  create_sa    = local.is_not_dev
  sa_name      = var.service_name
  namespace    = var.env_id
}
