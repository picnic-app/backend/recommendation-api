CREATE TABLE Searches
(
    UserID    STRING(36) NOT NULL,
    Kind      INT64      NOT NULL,
    EntityID  STRING(36) NOT NULL,
    CreatedAt TIMESTAMP  NOT NULL OPTIONS (allow_commit_timestamp = true),
) PRIMARY KEY (UserID, Kind, EntityID);
