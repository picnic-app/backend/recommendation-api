package service

import "context"

func (s *Service) AddPostClassificationTags(ctx context.Context, postID string, tags ...string) error {
	return s.repo.SingleWrite().UpsertPostClassificationTags(ctx, postID, tags...)
}
