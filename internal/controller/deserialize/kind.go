package deserialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/model"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/service"
)

func Kind(k v1.Kind) (int64, error) {
	switch k {
	case v1.Kind_KIND_APP:
		return model.KindApp, nil
	case v1.Kind_KIND_CHAT:
		return model.KindChat, nil
	case v1.Kind_KIND_POST:
		return model.KindPost, nil
	case v1.Kind_KIND_CIRCLE:
		return model.KindCircle, nil
	case v1.Kind_KIND_PROFILE:
		return model.KindProfile, nil
	default:
		return 0, service.ErrUnsupported("kind", k)
	}
}
