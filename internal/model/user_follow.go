package model

type UserUserEdge struct {
	UserID       string
	TargetUserID string
	Following    bool
	Blocking     bool
}
