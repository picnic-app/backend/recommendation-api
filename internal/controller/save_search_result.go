package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/controller/deserialize"
)

func (c Controller) SaveSearchResult(
	ctx context.Context,
	req *v1.SaveSearchResultRequest,
) (*v1.SaveSearchResultResponse, error) {
	kind, err := deserialize.Kind(req.GetKind())
	if err != nil {
		return nil, err
	}

	err = c.service.SaveSearchResult(ctx, kind, req.GetEntityKey().GetId())
	return &v1.SaveSearchResultResponse{}, err
}
