package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) PartialUpdateUserUserEdge(
	ctx context.Context,
	input repo.PartialUpdateUserUserEdgeInput,
) error {
	return PartialUpdateUserUserEdge(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), input)
}

func (rw readWrite) PartialUpdateUserUserEdge(
	ctx context.Context,
	input repo.PartialUpdateUserUserEdgeInput,
) error {
	return PartialUpdateUserUserEdge(ctx, rw.tx, input)
}

func PartialUpdateUserUserEdge(
	_ context.Context,
	db BufferWriter,
	input repo.PartialUpdateUserUserEdgeInput,
) error {
	table := tables.Get().UserUserEdges()

	hasUpdate := false
	kv := map[string]interface{}{
		table.Columns().UserID():       input.UserUserEdgeKey.UserID,
		table.Columns().TargetUserID(): input.UserUserEdgeKey.TargetUserID,
	}

	if input.FollowedAt != nil && input.UnfollowedAt != nil {
		return errors.New("cannot set both followed_at and unfollowed_at")
	}

	if input.FollowedAt != nil {
		kv[table.Columns().FollowedAt()] = input.FollowedAt
		hasUpdate = true
	}

	if input.UnfollowedAt != nil {
		kv[table.Columns().UnfollowedAt()] = input.UnfollowedAt
		hasUpdate = true
	}

	if input.BlockedAt != nil && input.UnblockedAt != nil {
		return errors.New("cannot set both blocked_at and unblocked_at")
	}

	if input.BlockedAt != nil {
		kv[table.Columns().BlockedAt()] = input.BlockedAt
		hasUpdate = true
	}

	if input.UnblockedAt != nil {
		kv[table.Columns().UnblockedAt()] = input.UnblockedAt
		hasUpdate = true
	}

	if !hasUpdate {
		return nil
	}

	return db.BufferWrite([]*spanner.Mutation{spanner.UpdateMap(
		table.TableName(),
		kv,
	)})
}
