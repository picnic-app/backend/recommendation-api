package spanner

import (
	"context"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetPosts(ctx context.Context, postIDs ...string) ([]repo.Post, error) {
	return GetPosts(ctx, r.tx, postIDs...)
}

func (rw readWrite) GetPosts(ctx context.Context, postIDs ...string) ([]repo.Post, error) {
	return GetPosts(ctx, rw.tx, postIDs...)
}

func GetPosts(ctx context.Context, db Reader, postIDs ...string) ([]repo.Post, error) {
	return GetByKeys[repo.Post](ctx, db, tables.Get().Posts(), postIDs...)
}
