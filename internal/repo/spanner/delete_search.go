package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) DeleteSearch(ctx context.Context, in repo.SearchKey) error {
	return DeleteSearch(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), in)
}

func (rw readWrite) DeleteSearch(ctx context.Context, in repo.SearchKey) error {
	return DeleteSearch(ctx, rw.tx, in)
}

func DeleteSearch(_ context.Context, db BufferWriter, in repo.SearchKey) error {
	return Delete(db, tables.Get().Searches(), spanner.Key{in.UserID, in.Kind, in.EntityID})
}
