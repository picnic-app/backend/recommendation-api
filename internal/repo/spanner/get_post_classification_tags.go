package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (r readOnly) GetPostClassificationTags(ctx context.Context, postID string) ([]string, error) {
	return GetPostClassificationTags(ctx, r.tx, postID)
}

func (rw readWrite) GetPostClassificationTags(ctx context.Context, postID string) ([]string, error) {
	return GetPostClassificationTags(ctx, rw.tx, postID)
}

func GetPostClassificationTags(ctx context.Context, db Reader, postID string) ([]string, error) {
	table := tables.Get().PostClassificationTags()
	iter := db.Read(
		ctx,
		table.TableName(),
		spanner.Key{postID}.AsPrefix(),
		[]string{table.Columns().ClassificationTag()},
	)
	return GetResults[string](iter)
}
