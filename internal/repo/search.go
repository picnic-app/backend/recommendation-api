package repo

import "time"

type SearchKey struct {
	Kind     Kind
	UserID   UserID
	EntityID EntityID
}

type SearchInput struct {
	SearchKey
}

type Search struct {
	SearchKey
	CreatedAt time.Time
}
