package migrations

import (
	"context"
	"time"

	"cloud.google.com/go/spanner"
	"go.uber.org/zap"

	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/migration"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
	spannerRepo "gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner"
)

func NewManager(db *spanner.Client, instanceID string) *Manager {
	return &Manager{
		db:         db,
		instanceID: instanceID,
		m:          migration.NewManager(db),
		repo:       spannerRepo.NewRepo(db),
	}
}

type Manager struct {
	instanceID string
	db         *spanner.Client
	m          *migration.Manager
	repo       repo.Repo
}

func (r *Manager) run(ctx context.Context, label string, run uint, f func(context.Context) error) {
	go func() {
		err := r.m.Run(ctx, label, run, r.instanceID, time.Minute, f)
		if err != nil {
			logger.ErrorKV(
				ctx,
				"running migration",
				zap.Error(err),
				zap.Uint("run", run),
				zap.String("label", label),
			)
		}
	}()
}
