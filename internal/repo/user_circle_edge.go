package repo

import (
	"time"
)

type UserCircleEdgeKey struct {
	UserID   UserID
	CircleID CircleID
}

type UserCircleEdge struct {
	UserCircleEdgeKey
	IsMember  bool
	JoinedAt  *time.Time
	LeftAt    *time.Time
	CreatedAt time.Time
}

type PartialUpdateUserCircleEdgeInput struct {
	UserCircleEdgeKey
	JoinedAt *time.Time
	LeftAt   *time.Time
}
