package service

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) DeleteCircle(
	ctx context.Context,
	circleID string,
	deletedAt time.Time,
) error {
	if circleID == "" {
		return ErrRequiredParam("circle id")
	}

	if deletedAt.IsZero() {
		return ErrRequiredParam("deleted at")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		circle, err := tx.GetCircle(ctx, repo.CircleKey{
			ID: circleID,
		})
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				return nil
			} else {
				return err
			}
		}

		if circle.LastUpdatedAt != nil && circle.LastUpdatedAt.After(deletedAt) {
			return nil
		}

		return tx.DeleteCircle(ctx, repo.CircleKey{
			ID: circleID,
		})
	})
	if err != nil {
		return err
	}

	return nil
}
