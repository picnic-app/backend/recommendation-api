// Code generated by tablegen. DO NOT EDIT.
package tables

func (t AllTables) DataMigrations() DataMigrations { return DataMigrations(t) }

type DataMigrations struct{ alias string }

func (t DataMigrations) TableAlias() string { return t.alias }
func (t DataMigrations) name() string       { return "DataMigrations" }
func (t DataMigrations) TableName() string  { return tableName(t.name(), "", t.alias) }
func (t DataMigrations) AllColumnNames() []string {
	c := t.Columns()
	return []string{
		c.Run(),
		c.Label(),
		c.InstanceId(),
		c.CompletedAt(),
		c.InstanceLockExpiresAt(),
	}
}

func (t DataMigrations) Columns() DataMigrationsCols { return DataMigrationsCols{tableAlias: t.alias} }

type DataMigrationsCols struct{ tableAlias string }

func (c DataMigrationsCols) Run() string         { return colName(c.tableAlias, "Run") }         // INT64
func (c DataMigrationsCols) Label() string       { return colName(c.tableAlias, "Label") }       // STRING(100)
func (c DataMigrationsCols) InstanceId() string  { return colName(c.tableAlias, "InstanceId") }  // STRING(36) NULL
func (c DataMigrationsCols) CompletedAt() string { return colName(c.tableAlias, "CompletedAt") } // TIMESTAMP NULL
func (c DataMigrationsCols) InstanceLockExpiresAt() string {
	return colName(c.tableAlias, "InstanceLockExpiresAt")
} // TIMESTAMP NULL
