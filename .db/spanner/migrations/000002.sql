CREATE TABLE TempUsers (
    Id STRING(36) NOT NULL,
    Username STRING(256) NOT NULL,
    Surname STRING(256),
    Bio STRING(2048),
    ProfileImageUrl STRING(256),
    IsVerified BOOL NOT NULL,
    IsBanned BOOL NOT NULL,
    CreatedAt TIMESTAMP OPTIONS (
        allow_commit_timestamp = true
        ),
    UpdatedAt TIMESTAMP OPTIONS (
        allow_commit_timestamp = true
        ),
    DeletedAt TIMESTAMP OPTIONS (
        allow_commit_timestamp = true
        ),
    Firstname STRING(256) NOT NULL DEFAULT (""),
    IsMigrated BOOL NOT NULL DEFAULT (FALSE),
    Version STRING(36),
    Timezone STRING(128),
    FollowerCount INT64 NOT NULL DEFAULT (0),
    Status STRING(20) DEFAULT ("active"),
    UserRole INT64 NOT NULL DEFAULT (0),
    AffiliateCode STRING(10),
) PRIMARY KEY(Id);

CREATE TABLE TempFollows (
    Id STRING(36) NOT NULL,
    UserId STRING(36) NOT NULL,
    FollowedUserId STRING(36) NOT NULL,
    CreatedAt TIMESTAMP OPTIONS (
        allow_commit_timestamp = true
        ),
) PRIMARY KEY(Id);

CREATE TABLE TempBlocks (
    Id STRING(36) NOT NULL,
    UserId STRING(36) NOT NULL,
    BlockedUserId STRING(36) NOT NULL,
    CreatedAt TIMESTAMP OPTIONS (
        allow_commit_timestamp = true
        ),
    DeletedAt TIMESTAMP OPTIONS (
        allow_commit_timestamp = true
        ),
) PRIMARY KEY(Id);

CREATE TABLE TempCircleMembers (
          CircleID STRING(36) NOT NULL,
          UserID STRING(36) NOT NULL,
          Role INT64 NOT NULL,
          JoinedAt TIMESTAMP NOT NULL OPTIONS (
              allow_commit_timestamp = true
              ),
          BannedAt TIMESTAMP OPTIONS (
              allow_commit_timestamp = true
              ),
          BannedTime INT64 DEFAULT (0),
          UserName STRING(256) NOT NULL DEFAULT (""),
          IsCreatedOnBan BOOL DEFAULT (FALSE),
          AddModerators BOOL DEFAULT (FALSE),
          MemberOptions JSON DEFAULT (NULL),
) PRIMARY KEY(CircleID, UserID);

CREATE TABLE TempCircles (
    ID STRING(36) NOT NULL,
    Name STRING(256) NOT NULL,
    Description STRING(2048) NOT NULL,
    ImageUrl STRING(1024),
    LanguageCode STRING(2) NOT NULL,
    Kind INT64 NOT NULL,
    GroupID STRING(36) NOT NULL,
    ParentID STRING(36),
    IsPrivate BOOL,
    IsHidden BOOL,
    MembersCount INT64 NOT NULL,
    ViewsCount INT64 NOT NULL,
    PostsCount INT64 NOT NULL,
    CreatedAt TIMESTAMP NOT NULL OPTIONS (
        allow_commit_timestamp = true
        ),
    RulesType INT64,
    RulesText STRING(2048),
    ChatID STRING(36) DEFAULT (NULL),
    DeletedAt TIMESTAMP DEFAULT (NULL),
    IsVerified BOOL DEFAULT (FALSE),
    Visibility INT64 DEFAULT (0),
    JoinRequests INT64 DEFAULT (0),
    SortedOn INT64 DEFAULT (0),
    ReportsCount INT64 DEFAULT (0),
    Options JSON DEFAULT (NULL),
    ImageFileURL STRING(1024) DEFAULT (NULL),
    CoverImageFileURL STRING(1024) DEFAULT (NULL),
    Migration INT64 DEFAULT (0),
    MigrationStatus INT64 DEFAULT (0),
    MetaWords JSON DEFAULT (NULL),
    UrlName STRING(256) DEFAULT (NULL),
) PRIMARY KEY(ID);

CREATE TABLE Circles (
    ID STRING(36) NOT NULL,
    GroupID STRING(36) NOT NULL
) PRIMARY KEY(ID);

CREATE TABLE Users (
    UserID STRING(36) NOT NULL,
    LanguageCodes  ARRAY<STRING(36)> NOT NULL,
    IsBanned BOOL NOT NULL AS (
        CASE
        WHEN BannedAt IS NULL THEN FALSE
        WHEN BannedAt IS NOT NULL AND UnbannedAt IS NULL THEN TRUE
        WHEN BannedAt IS NOT NULL AND UnbannedAt IS NOT NULL AND BannedAt > UnbannedAt THEN TRUE
        ELSE FALSE
        END
    ) STORED,
    BannedAt TIMESTAMP,
    UnbannedAt TIMESTAMP,
    CreatedAt TIMESTAMP NOT NULL,
) PRIMARY KEY(UserID);

CREATE TABLE UserCircleEdges (
    UserID STRING(36) NOT NULL,
    CircleID STRING(36) NOT NULL,
    IsMember BOOL NOT NULL AS (
        CASE
        WHEN JoinedAt IS NULL THEN FALSE
        WHEN JoinedAt IS NOT NULL AND LeftAt IS NULL THEN TRUE
        WHEN JoinedAt IS NOT NULL AND LeftAt IS NOT NULL AND JoinedAt > LeftAt THEN TRUE
        ELSE FALSE
        END
    ) STORED,
    LeftAt TIMESTAMP,
    JoinedAt TIMESTAMP,
    CreatedAt TIMESTAMP NOT NULL
) PRIMARY KEY(UserID, CircleID);

CREATE TABLE UserUserEdges (
    UserID STRING(36) NOT NULL,
    TargetUserID STRING(36) NOT NULL,
    IsFollowing BOOL NOT NULL AS (
        CASE
        WHEN FollowedAt IS NULL THEN FALSE
        WHEN FollowedAt IS NOT NULL AND UnfollowedAt IS NULL THEN TRUE
        WHEN FollowedAt IS NOT NULL AND UnfollowedAt IS NOT NULL AND FollowedAt > UnfollowedAt THEN TRUE
        ELSE FALSE
        END
    ) STORED,
    FollowedAt TIMESTAMP,
    UnfollowedAt TIMESTAMP,
    IsBlocking BOOL NOT NULL AS (
        CASE
        WHEN BlockedAt IS NULL THEN FALSE
        WHEN BlockedAt IS NOT NULL AND UnblockedAt IS NULL THEN TRUE
        WHEN BlockedAt IS NOT NULL AND UnblockedAt IS NOT NULL AND BlockedAt > UnblockedAt THEN TRUE
        ELSE FALSE
        END
    ) STORED,
    BlockedAt TIMESTAMP,
    UnblockedAt TIMESTAMP,
) PRIMARY KEY(UserID, TargetUserID);
