package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo"
)

func (s *Service) UnsaveSearchResult(ctx context.Context, kind int64, entityID string) error {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return err
	}

	if err = s.ValidateKind(ctx, kind); err != nil {
		return err
	}

	if err = s.ValidateEntityID(ctx, kind, entityID); err != nil {
		return err
	}

	return s.repo.SingleWrite().DeleteSearch(
		ctx,
		repo.SearchKey{
			Kind:     kind,
			UserID:   userID,
			EntityID: entityID,
		},
	)
}
