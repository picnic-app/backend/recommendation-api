package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner/tables"
)

func (w writeOnly) UpsertPostClassificationTags(ctx context.Context, postID string, tags ...string) error {
	return UpsertPostClassificationTags(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), postID, tags...)
}

func (rw readWrite) UpsertPostClassificationTags(ctx context.Context, postID string, tags ...string) error {
	return UpsertPostClassificationTags(ctx, rw.tx, postID, tags...)
}

func UpsertPostClassificationTags(_ context.Context, db BufferWriter, postID string, tags ...string) error {
	if len(tags) == 0 {
		return nil
	}

	table := tables.Get().PostClassificationTags()
	cols := []string{table.Columns().PostID(), table.Columns().ClassificationTag()}

	ops := make([]*spanner.Mutation, len(tags))
	for i, tag := range tags {
		ops[i] = spanner.InsertOrUpdate(table.TableName(), cols, []interface{}{postID, tag})
	}

	return db.BufferWrite(ops)
}
