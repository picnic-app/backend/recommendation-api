package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/controller/deserialize"
)

func (c Controller) UnsaveSearchResult(
	ctx context.Context,
	req *v1.UnsaveSearchResultRequest,
) (*v1.UnsaveSearchResultResponse, error) {
	kind, err := deserialize.Kind(req.GetKind())
	if err != nil {
		return nil, err
	}

	err = c.service.UnsaveSearchResult(ctx, kind, req.GetEntityKey().GetId())
	return &v1.UnsaveSearchResultResponse{}, err
}
