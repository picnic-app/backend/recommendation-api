package serialize

import v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"

func EntityID(s string) *v1.EntityKey { return &v1.EntityKey{Id: s} }
