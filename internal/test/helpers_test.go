package test

import (
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/spanner"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/controller"
	spannerRepo "gitlab.com/picnic-app/backend/recommendation-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/recommendation-api/internal/service"
)

var (
	bus        eventbus.Client
	spannerCli *spanner.Client
	httpServer *httptest.Server
	mux        = &http.ServeMux{}
)

type Container struct {
	mux         *http.ServeMux
	bus         eventbus.Client
	spannerCli  *spanner.Client
	service     *service.Service
	httpServer  *httptest.Server
	spannerRepo spannerRepo.Repo
	controller  controller.Controller
}

func (c Container) Close() {}

func initContainer(_ *testing.T, _ ...interface{}) Container {
	repo := spannerRepo.NewRepo(spannerCli)
	svc := service.New(repo)

	return Container{
		mux:         mux,
		bus:         bus,
		service:     svc,
		spannerRepo: repo,
		httpServer:  httpServer,
		spannerCli:  spannerCli,
		controller:  controller.New(svc),
	}
}

// isCI checks if the test is running on a CI environment.
func isCI() bool {
	value, found := os.LookupEnv("CI")
	if !found {
		return false
	}
	return strings.ToLower(value) != "false"
}

const (
	timeout = time.Minute / 2
	tick    = time.Second
)

func Receive[T any](t require.TestingT, ch <-chan T, msgAndArgs ...interface{}) (r T) {
	ticker := time.NewTicker(timeout)
	defer ticker.Stop()

	select {
	case r = <-ch:
	case <-ticker.C:
		require.Fail(t, "timeout has been exceeded", msgAndArgs...)
	}
	return r
}

func Eventually(t require.TestingT, cond func() bool, msgAndArgs ...interface{}) {
	require.Eventually(t, cond, timeout, tick, msgAndArgs...)
}

func Never(t require.TestingT, cond func() bool, msgAndArgs ...interface{}) {
	require.Never(t, cond, 5*time.Second, tick, msgAndArgs...)
}
