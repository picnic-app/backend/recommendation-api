package test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	common "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/picnic/common/v1"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/recommendation-api/recommendation/v1"
)

func TestGetSavedSearchResult(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID, appID := uuid.NewString(), uuid.NewString()

	ctx := auth.AddUserIDToCtx(context.Background(), userID)

	const kind = v1.Kind_KIND_APP

	_, err := container.controller.SaveSearchResult(
		ctx,
		&v1.SaveSearchResultRequest{
			Kind:      kind,
			EntityKey: &v1.EntityKey{Id: appID},
		},
	)
	require.NoError(t, err)

	resp, err := container.controller.GetSavedSearchResults(ctx, &v1.GetSavedSearchResultsRequest{Kind: kind})
	require.NoError(t, err)
	require.Len(t, resp.GetResults(), 1)
	require.Equal(t, kind, resp.GetResults()[0].GetKind())
	require.Equal(t, appID, resp.GetResults()[0].GetEntityKey().GetId())
}

func TestGetSavedSearchResult_Cursor(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()

	chat1, chat2 := uuid.NewString(), uuid.NewString()
	const kind = v1.Kind_KIND_CHAT
	saveSearch(t, container, kind, userID, chat1)
	saveSearch(t, container, kind, userID, chat2)

	ctx := auth.AddUserIDToCtx(context.Background(), userID)

	req := &v1.GetSavedSearchResultsRequest{Kind: kind, Cursor: &common.PageCursor{Limit: 1}}
	for {
		resp, err := container.controller.GetSavedSearchResults(ctx, req)
		require.NoError(t, err)

		if !resp.GetPageInfo().GetHasNext() {
			break
		}

		req.Cursor.LastId = resp.GetPageInfo().GetLastId()
	}

	req.Cursor.Dir = common.PageDir_PAGE_DIR_BACKWARD
	for {
		resp, err := container.controller.GetSavedSearchResults(ctx, req)
		require.NoError(t, err)

		if !resp.GetPageInfo().GetHasPrev() {
			break
		}

		req.Cursor.LastId = resp.GetPageInfo().GetFirstId()
	}
}

func TestGetSavedSearchResult_Validation(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	ctx := auth.AddUserIDToCtx(context.Background(), uuid.NewString())

	for _, test := range []struct {
		name string
		ctx  context.Context
		req  *v1.GetSavedSearchResultsRequest
		code codes.Code
	}{
		{
			name: "unauthenticated",
			ctx:  context.Background(),
			req: &v1.GetSavedSearchResultsRequest{
				Kind: v1.Kind_KIND_PROFILE,
			},
			code: codes.Unauthenticated,
		},
		{
			name: "invalid kind",
			ctx:  ctx,
			req: &v1.GetSavedSearchResultsRequest{
				Kind: v1.Kind_KIND_UNSPECIFIED,
			},
			code: codes.InvalidArgument,
		},
		{
			name: "ok",
			ctx:  ctx,
			req: &v1.GetSavedSearchResultsRequest{
				Kind: v1.Kind_KIND_POST,
			},
			code: codes.OK,
		},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			_, err := container.controller.GetSavedSearchResults(test.ctx, test.req)
			require.Equal(t, test.code, status.Code(err))
		})
	}
}

func saveSearch(t require.TestingT, container Container, kind v1.Kind, userID, entityID string) {
	_, err := container.controller.SaveSearchResult(
		auth.AddUserIDToCtx(context.Background(), userID),
		&v1.SaveSearchResultRequest{
			Kind:      kind,
			EntityKey: &v1.EntityKey{Id: entityID},
		},
	)
	require.NoError(t, err)
}
